#!/bin/bash

for file in lattests/good/*.lat; do ./latc $file; done
for file in lattests/bad/*.lat; do echo $file; ./latc $file; done

for file in lattests/good/*.class; do
	in=./lattests/good/$(basename ${file%.*}.input);
	if [ ! -f $in ];
	then
		in=./lattests/good/dumb.input;
	fi;
	diff -s \
	<(java  -classpath ":./lattests/good/:./bin/" $(basename ${file%.*}) < $in) \
	lattests/good/$(basename ${file%.*}).output;
done
