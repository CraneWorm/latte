#include <stdio.h>
#include <stdlib.h>

void error(){
	printf("runtime error\n");
	exit(1);
}

void printInt(int i){
	printf("%d\n", i);
}

void printString(char* string){
	printf("%s\n", string);
}

int readInt(){
	int i;
	scanf("%d ", &i);
	return i;
}

char* readString(){
	char* str = NULL;
	size_t size;
	getline(&str, &size, stdin);
	strtok(str, "\n");
	return str;
}
