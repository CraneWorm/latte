SHELL=/bin/bash
.PHONY: all clean test

default: all
all:
	ant

clean:
	ant clean

test:
	./test
