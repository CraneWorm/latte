package visitors;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import latte_program_tree.LatteProgram;

import org.junit.Test;

import auto.Lexer;
import auto.Parser;

public class ProgramTreePrinterTest {

	private InputStream streamFromString(String chars) throws UnsupportedEncodingException{
		return new ByteArrayInputStream(chars.getBytes("UTF-8"));
	}
	
	@SuppressWarnings("unused")
	private void print(Object o){
		System.out.println(o);
	}
	
	@Test
	public void testVisitLatteProgram() throws Exception {
		String stream = "int a(){iden[1] + 2;}";
		Parser parser = new Parser(new Lexer(streamFromString(stream)));
		LatteProgram P = (LatteProgram) parser.parse().value;
		
		ProgramTreePrinter printer = new ProgramTreePrinter();
		printer.visit(P);
		
	}

}
