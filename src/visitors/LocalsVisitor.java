package visitors;

import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteInstruction;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;

public class LocalsVisitor extends AbstractProgramNodeVisitor<Integer> {

	@Override
	public Integer visit(AssignInstruction assignInstruction) {
		return 0;
	}

	@Override
	public Integer visit(DotExpression dotExpression) {
		return 0;
	}

	@Override
	public Integer visit(NewExpression newExpression) {
		return 0;
	}

	@Override
	public Integer visit(BlockInstruction blockInstruction) {
		int total = 0;
		for (LatteInstruction stmt: blockInstruction.body){
			total += stmt.accept(this);
		}
		return total;
	}

	@Override
	public Integer visit(ClassDeclaration classDeclaration) {
		return 0;
	}

	@Override
	public Integer visit(ConstExpression constExpression) {
		return 0;
	}

	@Override
	public Integer visit(DecrementInstruction decrementInstruction) {
		return 0;
	}

	@Override
	public Integer visit(EmptyInstruction emptyInstruction) {
		return 0;
	}

	@Override
	public Integer visit(FunctionDeclaration functionDeclaration) {
		return functionDeclaration.argument_types.size() + 
				functionDeclaration.body.accept(this);
	}

	@Override
	public Integer visit(IfInstruction ifInstruction) {
		return ifInstruction.if_branch.accept(this) + 
				ifInstruction.else_branch.accept(this);
	}

	@Override
	public Integer visit(IncrementInstruction incrementInstruction) {
		return 0;
	}

	@Override
	public Integer visit(LatteProgram latteProgram) {
		return 0;
	}

	@Override
	public Integer visit(LatteVar latteVar) {
		return 1;
	}

	@Override
	public Integer visit(ReturnInstruction returnInstruction) {
		return 0;
	}

	@Override
	public Integer visit(VarDeclaration varDeclaration) {
		return varDeclaration.vars.size();
	}

	@Override
	public Integer visit(WhileInstruction whileInstruction) {
		return whileInstruction.loop.accept(this);
	}

	@Override
	public Integer visit(ForEach forEach) {
		return 0;
	}

	@Override
	public Integer visit(FunctionApplication functionApplication) {
		return 0;
	}

	@Override
	public Integer visit(VarExpression varExpression) {
		return 0;
	}

	@Override
	public Integer visit(AddExpression addExpression) {
		return 0;
	}

	@Override
	public Integer visit(SubExpression subExpression) {
		return 0;
	}

	@Override
	public Integer visit(MulExpression mulExpression) {
		return 0;
	}

	@Override
	public Integer visit(DivExpression divExpression) {
		return 0;
	}

	@Override
	public Integer visit(GeExpression geExpression) {
		return 0;
	}

	@Override
	public Integer visit(LeExpression leExpression) {
		return 0;
	}

	@Override
	public Integer visit(GthExpression gthExpression) {
		return 0;
	}

	@Override
	public Integer visit(LthExpression lthExpression) {
		return 0;
	}

	@Override
	public Integer visit(OrExpression orExpression) {
		return 0;
	}

	@Override
	public Integer visit(AndExpression andExpression) {
		return 0;
	}

	@Override
	public Integer visit(NeExpression neExpression) {
		return 0;
	}

	@Override
	public Integer visit(EquExpression equExpression) {
		return 0;
	}

	@Override
	public Integer visit(LatteType latteType) {
		return 0;
	}

	@Override
	public Integer visit(ModExpression modExpression) {
		return 0;
	}

	@Override
	public Integer visit(UminusExpression uminusExpression) {
		return 0;
	}

	@Override
	public Integer visit(NotExpression notExpression) {
		return 0;
	}

	@Override
	public Integer visit(ExpressionInstruction expressionInstruction) {
		return 0;
	}

	@Override
	public Integer visit(ArrayExpression arrayExpression) {
		return 0;
	}

	@Override
	public Integer visit(SelfExpression selfExpression) {
		return 0;
	}

	@Override
	public Integer visit(ArrayAllocation arrayAllocation) {
		// TODO Auto-generated method stub
		return null;
	}

}
