package visitors;

import latte_program_tree.LatteAddress;
import latte_program_tree.ProgramStructNode;
import helpers.ErrorReporter;

public abstract class AbstractProgramNodeVisitor<T> implements
		ProgramNodeVisitor<T> {
	protected ErrorReporter errorReporter;
	public ErrorReporter getErrorReporter(){return errorReporter;}
	public AbstractProgramNodeVisitor() {
		this.errorReporter = new ErrorReporter();
	}
	public AbstractProgramNodeVisitor<T> visit(ProgramStructNode program) {
		program.accept(this);
		return this;
	}
	public T visit(LatteAddress latteAddress){return null;}
}
