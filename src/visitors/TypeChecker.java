package visitors;

import helpers.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import java_cup.runtime.Symbol;
import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteAddress;
import latte_program_tree.LatteExpression;
import latte_program_tree.LatteFunctionType;
import latte_program_tree.LatteInstruction;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;


public class TypeChecker extends AbstractProgramNodeVisitor<LatteType> {
	public static final String LENGTH = "length";
	public static final LatteType VOID = new LatteType("void", null);
	public static final LatteType BOOL = new LatteType("boolean", null);
	public static final LatteType STRING = new LatteType("string", null);
	public static final LatteType INT = new LatteType("int", null);
	public static final LatteType ERROR = new LatteType("type error", null);
	public LinkedList<Scope> symbol_table;

	@SuppressWarnings("serial")
	public TypeChecker(){
		super();
		this.symbol_table = new LinkedList<Scope>(){
			public void push(Scope scope){
				if (this.peek() != null && scope.class_context == null){
					//scope.class_context = peek().class_context;
				}
				if (this.peek() != null && scope.function_context == null){
					scope.function_context = peek().function_context;
				}
				super.push(scope);
					
			}
		};
	}
	private void parse_error(String message, Symbol sym){
		if (symbol_table.peek() != null && symbol_table.peek().function_context != null){
			FunctionDeclaration fun = symbol_table.peek().function_context;
			String long_name = fun.function_name + fun.argument_types;
			errorReporter.report_error(message, long_name, sym);
			
		} else {
			errorReporter.report_error(message, "top level", sym);
		}
	}
	
	@SuppressWarnings("serial")
	public class Scope extends HashMap<String, LatteVar> {
		public FunctionDeclaration function_context;
		public ClassDeclaration class_context;
		public LatteVar put(String key, LatteVar value){
			if (super.containsKey(key)){
				parse_error("redeclared identifier: "+key, value.sym);
			} 
			return super.put(key, value);						
		}
		public Scope(){
			super();
		}
		
		public Scope(FunctionDeclaration function_context){
			super();
			this.function_context = function_context;			
		}
		public Scope(ClassDeclaration class_context){
			super();
			this.class_context = class_context;			
		}
	}
	
	public boolean is_a(LatteType A, LatteType B){
		///is A a subtype of B?
		if (A.equals(ERROR) || B.equals(ERROR)) return false;
		if (A.equals(B)) return true;
		ClassDeclaration ClassA = classes.get(A.name);
		ClassDeclaration ClassB = classes.get(B.name);
		if (ClassB == null) return false;
		while (ClassA != null){
			if (ClassA.class_name.equals(B.name)) return true;
			ClassA = classes.get(ClassA.parent_class);
		}
		return false;
	}
	
	public LatteType lookup(VarExpression var) {
		String ident = var.ident;
		LatteVar t = null;
		Scope sc = null;
		Iterator<Scope> scopes = symbol_table.iterator();
		while(scopes.hasNext() && 
				(t = (sc = scopes.next()).get(ident)) == null );
		if (t == null){
			parse_error("undeclared variable: "+ident, var.sym);
			symbol_table.getLast().put(ident, new LatteVar(ERROR, ident, var.sym));
			return ERROR;
		}
		var.type = t.type;
		if (sc.class_context != null){
			var.object = new SelfExpression(null);
			var.object.accept(this);
		}
		return t.type;		
	}
	
	public LatteFunctionType method_lookup(String class_name, String method_name){
		if (classes.containsKey(class_name)){
			ClassDeclaration cls = classes.get(class_name);
			while (cls != null){
				if (cls.methods.containsKey(method_name)){
					FunctionDeclaration fd = cls.methods.get(method_name);
					return fd.function_type;
				}
				cls = classes.get(cls.parent_class);
			}			
		}
		return null;
	}
	
	public LatteType attribute_lookup(LatteType type, String attribute_name){
		if (types.contains(type) || classes.containsKey(type.name)){
			if (type.name.endsWith("[]")) return (attribute_name.equals(LENGTH) ? INT : ERROR);
			ClassDeclaration cls = classes.get(type.name);
			while (cls != null){
				if (cls.attributes.containsKey(attribute_name)){
					return cls.attributes.get(attribute_name).type;
				}
				cls = classes.get(cls.parent_class);
			}			
			return ERROR;
		}
		return ERROR;		
	}
	
	@SuppressWarnings("serial")
	public final HashSet<LatteType> types = new HashSet<LatteType>(){
		@Override
		public boolean contains(Object t){
			if (!super.contains(t) && !classes.containsKey(((LatteType)t).name)){ 
				LatteType type = (LatteType)t;
				String name = type.name;
				if (name.endsWith("[]")){
					LatteType t2 = new LatteType(name.substring(0, name.lastIndexOf("[]")), null);
					if (contains(t2) || classes.containsKey(((LatteType)t2).name)){
						add(type);
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
			return true;			
		}
		{
			add (INT);
			add (BOOL);
			add (STRING);
			add (VOID);
			add (ERROR);
		}
	};
	
	@SuppressWarnings("serial")
	public HashMap<String, ClassDeclaration> classes = new HashMap<String, ClassDeclaration>(){
		@Override
		public ClassDeclaration put(String key, ClassDeclaration value){
			if (super.containsKey(key)){
				parse_error("redeclared class: "+key, null);					
			} 
			return super.put(key, value);	
		}
	};
	
		
	@SuppressWarnings("serial")
	public HashMap<String, LatteFunctionType> functions = new HashMap<String, LatteFunctionType>(){
		@Override
		public LatteFunctionType put(String key, LatteFunctionType value){
			if (super.containsKey(key)){
				parse_error("redeclared function: "+key, null);					
			} 
			return super.put(key, value);	
		}
		{
			put(
				"printInt",
				new LatteFunctionType(VOID,new ArrayList<LatteType>(){{add(INT);}})
			);
			put(
				"printString",
				new LatteFunctionType(VOID,new ArrayList<LatteType>(){{add(STRING);}})
			);
			put(
				"error",
				new LatteFunctionType(VOID, new ArrayList<LatteType>())
			);
			put("readInt", new LatteFunctionType(INT, new ArrayList<LatteType>()));
			put("readString", new LatteFunctionType(STRING, new ArrayList<LatteType>()));
		}
	};
	
	@Override
	public LatteType visit(DotExpression o) {
		return ERROR;		
	}

	@Override
	public LatteType visit(NewExpression o) {
		if (o.expr == null || !o.expr.accept(this).equals(ERROR)){
			return o.type;
		} else {
			return ERROR;
		}		
	}

	@Override
	public LatteType visit(ForEach o) {
		LatteType itertype = o.loop_var.accept(this);
		LatteType arraytype = o.array_expr.accept(this);
		LatteType innertype = new LatteType(arraytype.name.substring(0, arraytype.name.lastIndexOf("[]")), null);
		if (!is_a(innertype, itertype)){
			parse_error(
					String.format(
						"type of foreach placeholder variable must match type of the array"
					),
					o.sym
			);
			return ERROR;
		}
		symbol_table.push(new Scope(symbol_table.peek().function_context));
		symbol_table.peek().put(o.loop_var.ident, o.loop_var);
		o.loop_instruction.accept(this);
		symbol_table.pop();
		return VOID;
	}
	
	private LatteType visit(FunctionApplication o, LatteFunctionType t){
		if (t == null){
			parse_error("undefined function: "+ o.function_name,
			o.sym);
			return ERROR;
		}
		if (o.args.size() != t.argument_types.size()){
			parse_error(
				String.format(
					"function %s expects %d argument(s), %d passed",
					o.function_name,
					t.argument_types.size(),
					o.args.size()
				),
				o.sym
			);
			return t.return_type;
		}
		Iterator<LatteType> args = t.argument_types.iterator();
		for (LatteExpression e: o.args){
			LatteType tr = e.accept(this), tl = args.next();
			if (!is_a(tr, tl)){
				parse_error(
					String.format(
						"wrong type for argument, expected %s" +
						(tr.equals(ERROR) ? "" : ", got: %s"),						
						tl.name,
						tr.name
					),
					e.sym
				);
			}
		}
		return t.return_type;
	}
	
	@Override
	public LatteType visit(FunctionApplication o) {
		LatteFunctionType t = null;
				
		if (o.object != null){
			LatteType ctype = o.object.accept(this);
			if (!ctype.equals(ERROR)){
				t = method_lookup(ctype.name, o.function_name);				
			} else {
				return ERROR;
			}
		} else {
			Iterator<Scope> it = symbol_table.iterator();
			
			ClassDeclaration ctx = null;
			while(it.hasNext() && (ctx = it.next().class_context) == null);
			if (ctx != null){
				t = method_lookup(ctx.class_name, o.function_name);
				if (t != null){
					o.object = new SelfExpression(null);
					o.object.accept(this);
					return this.visit(o, t);
				}
			}
			t = functions.get(o.function_name);			
		}
		o.type = this.visit(o, t); 
		return o.type;
	}

	@Override
	public LatteType visit(VarExpression o) {
		if (o.object != null){
			LatteType ctype = o.object.accept(this);
			if (!ctype.equals(ERROR)){
				LatteType attr = attribute_lookup(ctype, o.ident);
				if (attr.equals(ERROR)){
					errorReporter.report_error(
						String.format(
							"attribute %s does not exist in type: %s",
							o.ident,
							ctype.name							
						),
						symbol_table.peek().function_context.function_name,
						o.sym
					);
				}
				o.type = attr;
				return attr;
			}
			return ERROR;
		}
		o.type = lookup(o);
		return o.type;
	}

	@Override
	public LatteType visit(AssignInstruction o) {
		LatteType tl = o.lval.accept(this);
		LatteType tr = o.rval.accept(this);
		if (!is_a(tr, tl) && !tl.equals(ERROR)){
			parse_error(
				String.format(
					"right side of assignment should be of type: %s" +
					(tr.equals(ERROR) ? "" : ", got: %s"),
					tl.name,
					tr.name
				),
				o.rval.sym);
			return ERROR;
		} else {
			return tl;
		}
	}

	@Override
	public LatteType visit(BlockInstruction o) {
		symbol_table.push(new Scope(symbol_table.peek().function_context));
		for (LatteInstruction i: o.body){
			i.accept(this);
		}
		symbol_table.pop();
		return VOID;
	}

	private int push_class_recursive(String class_name){
		int depth = 1;
		ClassDeclaration cls = classes.get(class_name);
		if (cls == null){
			errorReporter.report_error("class "+cls+" does not exist ",
					symbol_table.peek().class_context.class_name, 
					symbol_table.peek().class_context.sym);
			classes.put(class_name, new ClassDeclaration(class_name, null));
			return 0;
		}
		if (cls.parent_class != null){
			depth += push_class_recursive(cls.parent_class);
		}
		symbol_table.push(new Scope(cls));
		symbol_table.peek().class_context = cls;
		for (LatteVar var: cls.attributes.values()){
			symbol_table.peek().put(var.ident, var);
		}
		return depth;
	}
	
	
	@Override
	public LatteType visit(ClassDeclaration o) {
		int depth = push_class_recursive(o.class_name);
		
		for (LatteVar attr: o.attributes.values()){
			attr.accept(this);
		}
		for (FunctionDeclaration meth: o.methods.values()){
			meth.accept(this);
		}
		for (; depth-- > 0;){
			symbol_table.pop();
		}
		return null;
	}

	@Override
	public LatteType visit(DecrementInstruction o) {
		LatteType itype = o.var.accept(this);
		if (itype.equals(INT)){
			return o.var.accept(this);
		}
		parse_error(String.format(
				"operator -- expects value of type: %s" +
				(itype.equals(ERROR) ? "" : ", got: %s"),						
				INT.name,
				itype.name
			),			
			o.sym
		);
		return ERROR;
	}

	@Override
	public LatteType visit(EmptyInstruction o) {
		return null;		
	}

	@Override
	public LatteType visit(FunctionDeclaration o) {
		o.return_type.accept(this);
		Scope scope = new Scope(o);
		if (o.argument_types != null){
			for (LatteVar var: o.argument_types){
				var.accept(this);
				scope.put(var.ident, var);
			}
		}		
		symbol_table.push(scope);
		LatteType ret = o.body.accept(this);
		symbol_table.pop();
		return ret;
	}

	@Override
	public LatteType visit(IfInstruction o) {
		LatteType tcond = o.condition.accept(this);
		if (!tcond.equals(BOOL)){
			parse_error(
				String.format(
					"if condition should return value of type boolean" +
					(tcond.equals(ERROR) ? "" : ", got: %s"),
					tcond.name
				),
				o.sym
			);
		}
		o.if_branch.accept(this);
		o.else_branch.accept(this);
		return null;
	}

	@Override
	public LatteType visit(IncrementInstruction o) {
		LatteType itype = o.var.accept(this);
		if (itype.equals(INT)){
			return o.var.accept(this);
		}
		parse_error(String.format(
				"operator ++ expects value of type: %s" +
				(itype.equals(ERROR) ? "" : ", got: %s"),						
				INT.name,
				itype.name
			),			
			o.sym
		);
		return ERROR;
	}

	
	@Override
	public LatteType visit(LatteProgram o) {
		LatteType rett = null;
		for(FunctionDeclaration top: o.functions){
			functions.put(top.function_name, top.function_type);
		}
		for(ClassDeclaration top: o.classes){
			classes.put(top.class_name, top);
		}
		if (functions.get("main") == null ||
				!functions.get("main").return_type.equals(INT) ||
				(functions.get("main").argument_types != null &&
					functions.get("main").argument_types.size() > 0)){
			parse_error("must define function int main()", new Symbol(1,0,0));
		}
		for (FunctionDeclaration top: o.functions){
			if (top.accept(this).equals(ERROR)){
				rett = ERROR;
			}
		}
		for (ClassDeclaration top: o.classes){
			top.accept(this);
		}
		return rett;
	}

	@Override
	public LatteType visit(LatteVar o) {
		if (o.type.equals(VOID)){
			parse_error("variable of type void: "+o.ident, o.sym);
		}
		return o.type.accept(this);		
	}

	@Override
	public LatteType visit(ReturnInstruction o) {
		FunctionDeclaration function_context = symbol_table.peek().function_context;
		LatteType return_type = function_context.return_type;
		if (return_type.equals(VOID) && o.return_value != null){			
			parse_error(
				String.format(
					"returning a value from function %s of type void",
					function_context.function_name
				),
				o.sym
			);
		} else if(!return_type.equals(VOID) && o.return_value == null){			
			parse_error(
				String.format(
					"function %s must return a value of type: %s",
					function_context.function_name,
					return_type.name						
				),
				o.sym
			);		
		} else if (o.return_value != null){			
			LatteType rt = o.return_value.accept(this);
			if (!is_a(rt, return_type)){
				parse_error(
					String.format(
						"function %s must return a value of type: %s" +
						(rt.equals(ERROR) ? "" : ", got: %s"),
						function_context.function_name,
						return_type.name,
						rt.name
					),
					o.sym
				);
			}
		}
		return null;
	}

	@Override
	public LatteType visit(VarDeclaration o) {
		LatteType ltype = o.type.accept(this);
		if (ltype.equals(ERROR)){
			return ERROR;
		}
		if (o.type.equals(VOID)){
			parse_error("variable of type void: "+o.vars.get(0).left.ident, o.sym);
		}
		for (Pair<LatteVar, LatteExpression> item: o.vars){
			if(item.right != null){
				LatteType rtype = item.right.accept(this);
				if(!is_a(rtype, o.type)){
					parse_error("variable "+item.left.ident+", initializer should be of type: "+o.type.name, item.right.sym);
				}
			}
			symbol_table.peek().put(item.left.ident, item.left);
		}
		return o.type;
	}

	@Override
	public LatteType visit(WhileInstruction o) {
		LatteType tcond = o.condition.accept(this);
		if (!tcond.equals(BOOL)){
			parse_error(
				"loop condition should return value of type boolean" + 
						(tcond.equals(ERROR) ? "" : ", got " + tcond),
				o.sym
			);
		}
		return o.loop.accept(this);
	}

	@Override
	public LatteType visit(ConstExpression o) {
		return o.type;
	}

	@Override
	public LatteType visit(AddExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && (ltype.equals(INT)||ltype.equals(STRING))){
				e.type = ltype;
				return ltype;
		}
		parse_error("type mismatch for operator +", e.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(SubExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(INT)){
				return ltype;
		}
		parse_error("type mismatch for operator -", e.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(MulExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(INT)){
				return ltype;
		}
		parse_error("type mismatch for operator *", e.sym);
		return ERROR;		
	}


	@Override
	public LatteType visit(DivExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(INT)){
				return ltype;
		}
		parse_error("type mismatch for operator /", e.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(GeExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(INT)){
				return BOOL;			
		}
		parse_error("type mismatch for operator >=", e.sym);
		return ERROR;	
	}


	@Override
	public LatteType visit(LeExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(INT)){
				return BOOL;			
		}
		parse_error("type mismatch for operator <=", e.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(GthExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(INT)){
				return BOOL;			
		}
		parse_error("type mismatch for operator >", e.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(LthExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(INT)){
				return BOOL;			
		}
		parse_error("type mismatch for operator <", e.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(OrExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(BOOL)){
			return ltype;
		}	
		parse_error("type mismatch for operator ||", e.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(AndExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(BOOL)){
			return ltype;
		}	
		parse_error("type mismatch for operator &&", e.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(NeExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(VOID) || rtype.equals(VOID)){
			if (ltype.equals(VOID)){
				parse_error("void value in comparison", e.left.sym);
			}
			if (rtype.equals(VOID)){
				parse_error("void value in comparison", e.right.sym);
			}
			return ERROR;
		}		
		if (ltype.equals(rtype)){
			return BOOL;
		} else {
			parse_error("type mismatch for operator !=", e.sym);
			return ERROR;
		}
	}


	@Override
	public LatteType visit(EquExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(VOID) || rtype.equals(VOID)){
			if (ltype.equals(VOID)){
				parse_error("void value in comparison", e.left.sym);
			}
			if (rtype.equals(VOID)){
				parse_error("void value in comparison", e.right.sym);
			}
			return ERROR;
		}
		if (ltype.equals(rtype)){
			return BOOL;
		} else {
			parse_error("type mismatch for operator ==", e.sym);
			return ERROR;
		}
	}


	@Override
	public LatteType visit(LatteType latteType) {
		if(types.contains(latteType) || classes.containsKey(latteType.name)){
			return latteType;			
		}		
		parse_error("unknown type: "+latteType.name, latteType.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(ModExpression e) {
		LatteType 	ltype = e.left.accept(this),
				rtype = e.right.accept(this);
		if (ltype.equals(rtype) && ltype.equals(INT)){
				return ltype;
		}
		parse_error("type mismatch for operator %", e.sym);
		return ERROR;
	}


	@Override
	public LatteType visit(UminusExpression e) {
		LatteType 	rtype = e.right.accept(this);
		if (rtype.equals(INT)) {
			return INT;
		} else {
			parse_error("type mismatch for operator ", e.sym);
			return ERROR;
		}		
	}


	@Override
	public LatteType visit(NotExpression e) {
		LatteType 	rtype = e.right.accept(this);
		if (rtype.equals(BOOL)) {
			return BOOL;
		} else {
			parse_error("type mismatch for operator ", e.sym);
			return ERROR;
		}	
	}

	@Override
	public LatteType visit(ExpressionInstruction expressionInstruction) {
		return expressionInstruction.expression.accept(this);
	}

	@Override
	public LatteType visit(ArrayExpression arrayExpression) {
		LatteType index_type = arrayExpression.index.accept(this);
		LatteType array_type = arrayExpression.array.accept(this);
		if (index_type.equals(INT) && array_type.name.endsWith("[]")){
			arrayExpression.type = 
					new LatteType(array_type.name.substring(0, array_type.name.lastIndexOf("[]")), null); 
			return arrayExpression.type;
		} else {
			return ERROR;
		}
	}

	@Override
	public LatteType visit(SelfExpression selfExpression) {
		Iterator<Scope> it = symbol_table.iterator();
		Scope sc = null;
		while(it.hasNext() && (sc = it.next()).class_context == null);
		if (sc != null){
			selfExpression.type = new LatteType(sc.class_context.class_name, selfExpression.sym); 
			return selfExpression.type;
		}
		errorReporter.report_error(
			"self must never be used outside the class context",
			symbol_table.peek().function_context.function_name,
			selfExpression.sym);	
		return ERROR;
	}
	
	public LatteType visit(LatteAddress latteAddress) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public LatteType visit(ArrayAllocation arrayAllocation) {
		LatteType itype = arrayAllocation.size_expression.accept(this);
		if (!itype.equals(INT)){
			errorReporter.report_error(
					"array size expression must return an int",
					symbol_table.peek().function_context.function_name,
					arrayAllocation.sym);
		}
		return arrayAllocation.type;
	}
}
