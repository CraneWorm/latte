package visitors;



import helpers.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteAddress;
import latte_program_tree.LatteExpression;
import latte_program_tree.LatteFunctionType;
import latte_program_tree.LatteInstruction;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;
import static visitors.JasminCodeGenerator.Context.*;

public class JasminCodeGenerator extends AbstractProgramNodeVisitor<Void> {
	private StringBuilder jasmin;
	
	private String concat(String separator, Object... strings){
		StringBuilder sb = new StringBuilder();

		for(Object item: strings){
		    sb.append(item.toString());
		    sb.append(separator);
		}

		if (strings.length > 0) {
		    sb.delete(sb.length() - separator.length(), sb.length());
		}
		return sb.toString();
	}
	
	private String class_name;
	private String source_name;
	
	private void _( Object... line){
		jasmin.append(concat(" ", line)+"\n");
	}
	
	private void comment(String line){ jasmin.append(";"+line+"\n"); lc++;}
	private void LABEL(String lab){_(lab+":");}
	private String label(){ return "L"+lc++; }
	private static final String SOURCE = ".source";
	private static final String CLASS = ".class";
	private static final String SUPER = ".super";

	private static final String LIMIT = ".limit";
	private static final String STACK = "stack";
	private static final String LOCALS = "locals";

	private static final String METHOD = ".method";
	private static final String END_METHOD = ".end method";
	
	private static final String PUBLIC = "public";
	private static final String STATIC = "static";

	private static final String INVOKESTATIC = "invokestatic";
	private static final String INVOKESPECIAL = "invokespecial";
	private static final String INVOKEVIRTUAL = "invokevirtual";
	
	private static final String ALOAD_0 = "aload_0";
	private static final String ILOAD_0 = "iload_0";
	private static final String IADD = "iadd";
	private static final String ISUB = "isub";
	private static final String IMUL = "imul";
	private static final String IDIV = "idiv";
	private static final String IREM = "irem";
	
	private static final String RETURN = "return";
	private static final String INEG = "ineg";
	private static final String IFGT = "ifgt";
	private static final String IFLT = "iflt";
	private static final String GOTO = "goto";
	private static final String IRETURN = "ireturn";
	private static final String ARETURN = "areturn";
	private static final String SUB = "sub";
	private static final String ICONST_1 = "iconst_1";
	private static final String POP = "pop";
	private static final String LDC = "ldc";
	private static final String IINC = "iinc";
	private static final String ILOAD = "iload";
	private static final String ILOAD_1 = "iload_1";
	private static final String ALOAD = "aload";
	private static final String IF_ICMPLT = "if_icmplt";
	private static final String IF_ICMPGT = "if_icmpgt";
	private static final String IF_ICMPLE = "if_icmple";
	private static final String IF_ICMPGE = "if_icmpge";
	private static final String IF_ICMPEQ = "if_icmpeq";
	private static final String IF_ICMPNE = "if_icmpne";
	private static final String IFEQ = "ifeq";
	private static final String ICONST_0 = "iconst_0";
	private static final String ASTORE = "astore";
	private static final String ISTORE = "istore";
	private static final String BIPUSH = "bipush";
	private static final String SIPUSH = "sipush";
	private static final String NEW = "new";
	private static final String DUP = "dup";
	private static final String NOP = "nop";
	
	private void preamble(){
		_( SOURCE, source_name );
		_( CLASS, PUBLIC, class_name );
		_( SUPER, "java/lang/Object" );
		_( METHOD, PUBLIC, "<init>()V" );
		_( LIMIT, STACK, 1 );
		_( LIMIT, LOCALS, 1);
		_( ALOAD_0 );
		_( INVOKESPECIAL, "java/lang/Object/<init>()V" );
		_( RETURN );
		_( END_METHOD );
		_( METHOD, PUBLIC, STATIC, "main([Ljava/lang/String;)V");
		_( INVOKESTATIC, class_name+"/MAIN()I");
		_( INVOKESTATIC, "java/lang/System.exit(I)V");
		_( RETURN );
		_( END_METHOD );
	}
	
	private int lc = 0;
	private static final LatteType VOID = new LatteType("void", null);
	private static final LatteType BOOL = new LatteType("boolean", null);
	private static final LatteType STRING = new LatteType("string", null);
	private static final LatteType INT = new LatteType("int", null);
	private static LinkedList<String> label_true = new LinkedList<String>();
	private static LinkedList<String> label_false = new LinkedList<String>();
	private FunctionDeclaration function_context;
	private Map<String, LatteFunctionType> functions;
	protected enum Context {
		CONDITION, VALUE, NONE, AND, OR
	}
	
	private LinkedList<Context> ctx = new LinkedList<Context>(){{ push(NONE); }};
	
	public String code(){return jasmin.toString();}
	@SuppressWarnings("serial")
	public JasminCodeGenerator(String source_name, String class_name){
		jasmin = new StringBuilder();
		functions = new HashMap<String, LatteFunctionType>(){{
			put(
				"printInt",
				new LatteFunctionType("Runtime", VOID,new ArrayList<LatteType>(){{add(INT);}})
			);
			put(
				"printString",
				new LatteFunctionType("Runtime", VOID,new ArrayList<LatteType>(){{add(STRING);}})
			);
			put(
				"error",
				new LatteFunctionType("Runtime", VOID, new ArrayList<LatteType>())
			);
			put("readInt", new LatteFunctionType("Runtime", INT, new ArrayList<LatteType>()));
			put("readString", new LatteFunctionType("Runtime", STRING, new ArrayList<LatteType>()));
		}};
		this.class_name = class_name;
		this.source_name = source_name;
		preamble();
	}
	
	@Override
	public Void visit(LatteProgram o) {
		o.accept(new JasminRewriter());
		for (FunctionDeclaration fun: o.functions){
			this.functions.put(fun.function_name, fun.function_type);
		}
		for (FunctionDeclaration fun: o.functions){
			fun.accept(this);
		}		
		return null;
	}

	@Override
	public Void visit(AssignInstruction o) {
		ctx.push(VALUE);
		LatteAddress lval = (LatteAddress)o.lval;
		int ADDR = lval.address;
		switch(lval.type.name){
			case "string":
				o.rval.accept(this);
				_(ASTORE, ADDR);
				break;
			case "int":
				o.rval.accept(this);
				_(ISTORE, ADDR);
				break;
			case "boolean":
				o.rval.accept(this);
				_(ISTORE, ADDR);
				break;
		}
		ctx.pop();
		return null;
	}

	@Override
	public Void visit(DotExpression o) {
		return null;
	}

	@Override
	public Void visit(NewExpression o) {
		return null;
	}

	@Override
	public Void visit(BlockInstruction o) {
		for (LatteInstruction stm: o.body){
			stm.accept(this);
		}
		return null;
	}

	@Override
	public Void visit(ClassDeclaration o) {
		return null;
	}

	@Override
	public Void visit(ConstExpression o) {
		switch(o.type.name){
			case "int":
				_(LDC, o.value);
				break;
			case "boolean":
				switch(ctx.peek()){
					case CONDITION: case AND: case OR:
						if ((boolean)o.value){
							_(GOTO, label_true.peek());
						} else {
							_(GOTO, label_false.peek());
						}
						break;
					default:
						if ((boolean)o.value){
							_(ICONST_1);
						} else {
							_(ICONST_0);
						}
						break;
				}
				break;
			case "string":
				_(LDC, '"'+((String)o.value)
						.replaceAll("([\n])", "\\\\n")
						.replaceAll("\\r","\\\\r")
						.replaceAll("\\t","\\\\t")
						.replaceAll("\\\"", "\\\\\"") + '"');
				break;
		}
		return null;
	}

	@Override
	public Void visit(DecrementInstruction o) {
		int ADDR = ((LatteAddress)o.var).address;
		_(IINC, ADDR, -1);
		return null;
	}

	@Override
	public Void visit(EmptyInstruction o) {
		return null;
	}

	@Override
	public Void visit(FunctionDeclaration o) {
		function_context = o;
		int stack = o.accept(new StackDepthVisitor());
		int locals = o.accept(new LocalsVisitor());
		_( METHOD, PUBLIC, STATIC, jasmin_function_type(o.function_name) );
		_( LIMIT, STACK, stack );
		_( LIMIT, LOCALS, locals );		
		o.body.accept(this);
		if (o.function_type.return_type.equals(VOID)){
			_(RETURN);
		}
		_( END_METHOD );
		function_context = null;
		return null;
	}

	@Override
	public Void visit(IfInstruction o) {
		String IF_TRUE = label();
		String IF_FALSE = label();
		String IF_END = label();
		boolean returns = new ReturnChecker().visit(o);
		label_true.push(IF_TRUE);
		label_false.push(IF_FALSE);
		ctx.push(CONDITION);
		o.condition.accept(this);
		ctx.pop();
		label_true.pop();
		label_false.pop();
		LABEL(IF_TRUE);
		o.if_branch.accept(this);
		if(!returns)
			_(GOTO, IF_END);
		LABEL(IF_FALSE);
		o.else_branch.accept(this);
		if(!returns)
			LABEL(IF_END);
		return null;
	}

	@Override
	public Void visit(IncrementInstruction o) {
		int ADDR = ((LatteAddress)o.var).address;
		_(IINC, ADDR, 1);
		return null;
	}

	@Override
	public Void visit(LatteVar o) {
		return null;
	}

	@Override
	public Void visit(ReturnInstruction o) {
		if (o.return_value != null){
			switch (function_context.return_type.name){
				case "string":
					o.return_value.accept(this);
					_(ARETURN);
					break;
				case "int":
					o.return_value.accept(this);
					_(IRETURN);
					break;
				case "boolean":
					ctx.push(VALUE);
					o.return_value.accept(this);
					ctx.pop();
					_(IRETURN);
					break;
			}
		} else {
			_(RETURN);
		}
		
		return null;
	}

	@Override
	public Void visit(VarDeclaration o) {
		for (Pair<LatteVar, LatteExpression> var: o.vars){
			if (var.right != null){
				var.right.accept(this);
				switch(o.type.name){
					case "int": case "boolean":
						_(ISTORE, var.left.ident);
					break;
					case "string":
						_(ASTORE, var.left.ident);
					break;
				}
			} else {
				switch(o.type.name){
					case "int": case "boolean":
						_(ICONST_0);
						_(ISTORE, var.left.ident);
					break;
					case "string":
						_(LDC, "\"\"");
						_(ASTORE, var.left.ident);
					break;
				}
			}
		}
		return null;
	}

	@Override
	public Void visit(WhileInstruction o) {
		String WHILE_LOOP = label();
		String WHILE_COND = label();
		String WHILE_END = label();
		_(GOTO, WHILE_COND);
		LABEL(WHILE_LOOP);
		o.loop.accept(this);
		LABEL(WHILE_COND);
		label_true.push(WHILE_LOOP);
		label_false.push(WHILE_END);
		ctx.push(CONDITION);
		o.condition.accept(this);
		ctx.pop();
		label_true.pop();
		label_false.pop();
		LABEL(WHILE_END);		
		return null;
	}

	@Override
	public Void visit(ForEach o) {
		return null;
	}
	
	private String jasmin_type(String type_name){
		switch(type_name){
			case "string":
				return "Ljava/lang/String;";
			case "int":
				return "I";
			case "boolean":
				return "Z";
			case "void":
				return "V";
		}
		throw new Error("this never happened");
	}
	
	private String jasmin_function_type(String function_name){
		LatteFunctionType ftype = functions.get(function_name);
		String types = "";
		if (ftype.argument_types != null){
			for (LatteType type: ftype.argument_types){
				types += jasmin_type(type.name);
			}
		}
		String fname = String.format(
				"%s(%s)%s",
				function_name,
				types,
				jasmin_type(ftype.return_type.name)
				);
		
		return fname;
	}
	@Override
	public Void visit(FunctionApplication o) {
		LatteFunctionType ftype = functions.get(o.function_name);
		for (LatteExpression arg: o.args){
			ctx.push(VALUE);
			arg.accept(this);
			ctx.pop();
		}
		String invoke_prefix = ((ftype.origin != null) ? ftype.origin : class_name)+"/";
		String FUNCTION = invoke_prefix + jasmin_function_type(o.function_name);
		_(INVOKESTATIC, FUNCTION);
		switch(ctx.peek()){
			case CONDITION: case AND: case OR:
				_(IFEQ, label_false.peek());
				_(GOTO, label_true.peek());
				break;
			default:
				
				break;
		}
		if (o.function_name.equals("error")){
			switch (function_context.return_type.name){
				case "string":					
					_(LDC, "\"\"");
					_(ARETURN);
					break;
				case "int": case "boolean":
					_(BIPUSH, 0);
					_(IRETURN);
					break;
				}
			return null;
		}
		return null;
	}

	@Override
	public Void visit(VarExpression o) {
		int addr = ((LatteAddress)o).address;
		String type = ((LatteAddress)o).type.name;
		switch(type){
			case "int": case "boolean":
				_(ILOAD, addr);
				break;
			case "string":
				_(ALOAD, addr);
				break;
		}		
		return null;
	}

	@Override
	public Void visit(LatteAddress o) {
		int addr = o.address;
		String type = o.type.name;
		switch(type){
			case "int":
				_(ILOAD, addr);
				break;
			case "boolean":
				_(ILOAD, addr);
				switch(ctx.peek()){
					case CONDITION: case AND: case OR:
						_(IFEQ, label_false.peek());
						_(GOTO, label_true.peek());
						break;
					default:
						
						break;
				}
				break;
			case "string":
				_(ALOAD, addr);
				break;
		}		
		return null;
	}

	@Override
	public Void visit(AddExpression o) {
		if(o.type.equals(INT)){
			o.left.accept(this);
			o.right.accept(this);
			_(IADD);
		} else {
			_(NEW, "java/lang/StringBuilder");
			_(DUP);
			_(INVOKESPECIAL, "java/lang/StringBuilder/<init>()V");
			o.left.accept(this);
			_(INVOKEVIRTUAL, "java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;");
			o.right.accept(this);
			_(INVOKEVIRTUAL, "java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;");
			_(INVOKEVIRTUAL, "java/lang/StringBuilder/toString()Ljava/lang/String;");
		}
		return null;
	}

	@Override
	public Void visit(SubExpression o) {
		o.left.accept(this);
		o.right.accept(this);
		_(ISUB);
		return null;
	}

	@Override
	public Void visit(MulExpression o) {
		o.left.accept(this);
		o.right.accept(this);
		_(IMUL);
		return null;
	}

	@Override
	public Void visit(DivExpression o) {
		o.left.accept(this);
		o.right.accept(this);
		_(IDIV);
		return null;
	}

	@Override
	public Void visit(GeExpression o) {
		cond_pre();
		o.left.accept(this);
		o.right.accept(this);
		_(IF_ICMPLT, label_false.peek());
		_(GOTO, label_true.peek());
		cond_post();
		return null;
	}

	@Override
	public Void visit(LeExpression o) {
		cond_pre();
		o.left.accept(this);
		o.right.accept(this);
		_(IF_ICMPGT, label_false.peek());
		_(GOTO, label_true.peek());
		cond_post();
		return null;
	}

	@Override
	public Void visit(GthExpression o) {
		cond_pre();
		o.left.accept(this);
		o.right.accept(this);
		_(IF_ICMPLE, label_false.peek());
		_(GOTO, label_true.peek());
		cond_post();
		return null;
	}

	@Override
	public Void visit(LthExpression o) {
		cond_pre();
		o.left.accept(this);
		o.right.accept(this);
		_(IF_ICMPGE, label_false.peek());
		_(GOTO, label_true.peek());
		cond_post();
		return null;
	}

	@Override
	public Void visit(OrExpression o) {
		cond_pre();
		String LABEL_FALSE = label();
		label_false.push(LABEL_FALSE);
		ctx.push(OR);
		o.left.accept(this);
		ctx.pop();
		label_false.pop();
		LABEL(LABEL_FALSE);
		ctx.push(OR);
		o.right.accept(this);
		ctx.pop();
		cond_post();
		return null;
	}

	@Override
	public Void visit(AndExpression o) {
		cond_pre();
		String LABEL_TRUE = label();
		label_true.push(LABEL_TRUE);
		ctx.push(AND);
		o.left.accept(this);
		ctx.pop();
		label_true.pop();
		LABEL(LABEL_TRUE);
		ctx.push(AND);
		o.right.accept(this);
		ctx.pop();
		cond_post();
		return null;
	}

	@Override
	public Void visit(NeExpression o) {
		cond_pre();
		ctx.push(VALUE);
		o.left.accept(this);
		o.right.accept(this);
		ctx.pop();
		_(IF_ICMPEQ, label_false.peek());
		_(GOTO, label_true.peek());
		cond_post();
		return null;
	}

	@Override
	public Void visit(EquExpression o) {
		cond_pre();
		ctx.push(VALUE);
		o.left.accept(this);
		o.right.accept(this);
		ctx.pop();
		_(IF_ICMPNE, label_false.peek());
		_(GOTO, label_true.peek());
		cond_post();
		return null;
	}

	@Override
	public Void visit(LatteType o) {
		return null;
	}

	@Override
	public Void visit(ModExpression o) {
		o.left.accept(this);
		o.right.accept(this);
		_(IREM);
		return null;
	}

	@Override
	public Void visit(UminusExpression o) {
		o.right.accept(this);
		_(INEG);
		return null;
	}

	@Override
	public Void visit(NotExpression o) {
		cond_pre();
		String lab = label_true.pop();
		label_true.push(label_false.pop());
		label_false.push(lab);
		o.right.accept(this);
		label_false.pop();
		label_false.push(label_true.pop());
		label_true.push(lab);
		cond_post();
		return null;
	}
	
	private void cond_pre(){
		if (ctx.peek() == VALUE || ctx.peek() == NONE){
			label_false.push(label());
			label_true.push(label());
		}
	}
	
	private void cond_post(){
		if (ctx.peek() == VALUE || ctx.peek() == NONE){
			String END = label();
			LABEL(label_false.pop());
			_(ICONST_0);
			_(GOTO, END);
			LABEL(label_true.pop());
			_(ICONST_1);
			LABEL(END);
		}		
	}
	private boolean returns_void(LatteExpression o){
		if (o instanceof FunctionApplication){
			FunctionApplication fa = (FunctionApplication)o;
			LatteFunctionType ftype = functions.get(fa.function_name);
			return ftype.return_type.equals(VOID);
		}
		return false;
	}
	
	@Override
	public Void visit(ExpressionInstruction o) {
		o.expression.accept(this);
		if (!returns_void(o.expression)){
			_(POP);
		}
		return null;
	}

	@Override
	public Void visit(ArrayExpression arrayExpression) {
		return null;
	}

	@Override
	public Void visit(SelfExpression selfExpression) {
		return null;
	}

	@Override
	public Void visit(ArrayAllocation arrayAllocation) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
