package visitors;

import helpers.Pair;
import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteAddress;
import latte_program_tree.LatteExpression;
import latte_program_tree.LatteInstruction;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;

public class StackDepthVisitor extends AbstractProgramNodeVisitor<Integer> {

	private static final LatteType INT = new LatteType("int", null);

	@Override
	public Integer visit(AssignInstruction assignInstruction) {
		return assignInstruction.rval.accept(this);				
	}

	@Override
	public Integer visit(DotExpression dotExpression) {
		return 0;
	}

	@Override
	public Integer visit(NewExpression newExpression) {
		return 0;
	}

	@Override
	public Integer visit(BlockInstruction blockInstruction) {
		int max_d = 0;
		for (LatteInstruction stmt: blockInstruction.body){
			int stmt_d = stmt.accept(this);
			if (stmt_d > max_d) max_d = stmt_d;
		}
		return max_d;
	}

	@Override
	public Integer visit(ClassDeclaration classDeclaration) {
		return 0;
	}

	@Override
	public Integer visit(ConstExpression constExpression) {
		return 1;
	}

	@Override
	public Integer visit(DecrementInstruction decrementInstruction) {
		return 1;
	}

	@Override
	public Integer visit(EmptyInstruction emptyInstruction) {
		return 0;
	}

	@Override
	public Integer visit(FunctionDeclaration functionDeclaration) {
		return functionDeclaration.body.accept(this);
	}

	@Override
	public Integer visit(IfInstruction ifInstruction) {
		int cond_d = ifInstruction.condition.accept(this);
		if (ifInstruction.condition instanceof VarExpression || 
				ifInstruction.condition instanceof ConstExpression ||
				ifInstruction.condition instanceof LatteAddress){
			cond_d++;
		}
		int if_d = ifInstruction.if_branch.accept(this);
		int else_d = ifInstruction.else_branch.accept(this);
		return (if_d > else_d) ? 
				((if_d > cond_d) ? if_d : cond_d) :
				((else_d > cond_d) ? else_d : cond_d);				
	}

	@Override
	public Integer visit(IncrementInstruction incrementInstruction) {
		return 1;
	}

	@Override
	public Integer visit(LatteProgram latteProgram) {
		return null;
	}

	@Override
	public Integer visit(LatteVar latteVar) {
		return 1;
	}

	@Override
	public Integer visit(ReturnInstruction returnInstruction) {
		return (returnInstruction.return_value != null) ?
				returnInstruction.return_value.accept(this) : 0;				
	}

	@Override
	public Integer visit(VarDeclaration varDeclaration) {
		int max_d = 0;
		for (Pair<LatteVar, LatteExpression> var: varDeclaration.vars){
			if (var.right != null){
				int arg_d = var.right.accept(this);
				if (arg_d > max_d) max_d = arg_d;
			}
		}
		return max_d;		
	}

	@Override
	public Integer visit(WhileInstruction whileInstruction) {
		int cond_d = whileInstruction.condition.accept(this);
		int loop_d = whileInstruction.loop.accept(this);
		return cond_d > loop_d ? cond_d : loop_d;
	}

	@Override
	public Integer visit(ForEach forEach) {
		return 0;
	}

	@Override
	public Integer visit(FunctionApplication functionApplication) {
		int max_d = 1;//void functions that don't take arguments are overestimated here
		int arg_num = 0;
		for (LatteExpression arg: functionApplication.args){
			int arg_d = arg.accept(this);
			if (arg_d >= max_d - arg_num){  
				max_d = arg_d + arg_num;
			}
			arg_num++;
		}
		
		return max_d;
	}

	@Override
	public Integer visit(VarExpression varExpression) {
		return 1;// I optimise for the core version without extensions
	}

	@Override
	public Integer visit(AddExpression addExpression) {
		if (addExpression.type.equals(INT)){
			int dleft = addExpression.left.accept(this),
					dright = addExpression.right.accept(this);
			return (dleft > dright) ? dleft : dright + 1;
		}
		int dleft = addExpression.left.accept(this) + 1,
				dright = addExpression.right.accept(this) + 1;
			return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(SubExpression subExpression) {
		int dleft = subExpression.left.accept(this),
				dright = subExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(MulExpression mulExpression) {
		int dleft = mulExpression.left.accept(this),
				dright = mulExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(DivExpression divExpression) {
		int dleft = divExpression.left.accept(this),
				dright = divExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(GeExpression geExpression) {
		int dleft = geExpression.left.accept(this),
				dright = geExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(LeExpression leExpression) {
		int dleft = leExpression.left.accept(this),
				dright = leExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(GthExpression gthExpression) {
		int dleft = gthExpression.left.accept(this),
				dright = gthExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(LthExpression lthExpression) {
		int dleft = lthExpression.left.accept(this),
				dright = lthExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(OrExpression orExpression) {
		int dleft = orExpression.left.accept(this),
				dright = orExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright;
	}

	@Override
	public Integer visit(AndExpression andExpression) {
		int dleft = andExpression.left.accept(this),
				dright = andExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright;
	}

	@Override
	public Integer visit(NeExpression neExpression) {
		int dleft = neExpression.left.accept(this),
				dright = neExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(EquExpression equExpression) {
		int dleft = equExpression.left.accept(this),
				dright = equExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;		
	}

	@Override
	public Integer visit(LatteType latteType) {
		return 0;
	}

	@Override
	public Integer visit(ModExpression modExpression) {
		int dleft = modExpression.left.accept(this),
				dright = modExpression.right.accept(this);
		return (dleft > dright) ? dleft : dright + 1;
	}

	@Override
	public Integer visit(UminusExpression uminusExpression) {
		return uminusExpression.right.accept(this);
	}

	@Override
	public Integer visit(NotExpression notExpression) {
		int d = notExpression.right.accept(this);
		return (d >= 2) ? d : 2;
	}

	@Override
	public Integer visit(ExpressionInstruction expressionInstruction) {
		return expressionInstruction.expression.accept(this);
	}

	@Override
	public Integer visit(ArrayExpression arrayExpression) {
		return 0;
	}

	@Override
	public Integer visit(SelfExpression selfExpression) {
		return 0;
	}
	
	public Integer visit(LatteAddress latteAddress) {
		return 1;
	}

	@Override
	public Integer visit(ArrayAllocation arrayAllocation) {
		// TODO Auto-generated method stub
		return null;
	}

}
