package visitors;

import static org.junit.Assert.fail;
import latte_program_tree.AssignInstruction;
import latte_program_tree.ConstExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.IfInstruction;
import latte_program_tree.LatteInstruction;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.VarExpression;

import org.junit.Test;

public class ExpressionRewriterTest {

	@Test
	public void testVisitAssignInstruction() {
		LatteInstruction assign = new
				AssignInstruction(
						new VarExpression("x", null),
						new ConstExpression(
							"int",
							new Integer(7),
							null),
						null
						);
		assign.accept(new ExpressionRewriter());
		
	}

	@Test
	public void testVisitConstExpression() {
		ConstExpression b = new ConstExpression("boolean", new Boolean(true), null);
		ConstExpression i = new ConstExpression("int", new Integer(9), null);
		ConstExpression s = new ConstExpression("string", new String("strings are awsum"), null);
		ExpressionRewriter er = new ExpressionRewriter();
		b.accept(er);
		i.accept(er);
		s.accept(er);
		
	}

	@Test
	public void testVisitIfInstruction() {
		IfInstruction iff = new IfInstruction(
				new ConstExpression("boolean", new Boolean(true), null),
				new EmptyInstruction(null),
				new EmptyInstruction(null),
				null);
		iff.accept(new ExpressionRewriter());
	}

	@Test
	public void testVisitReturnInstruction() {
		ExpressionRewriter er = new ExpressionRewriter();
		ReturnInstruction rvalue = new ReturnInstruction(
				new ConstExpression(
						"int",
						new Integer(0),
						null),
				null);
		rvalue.accept(er);
		ReturnInstruction rvoid = new ReturnInstruction(null, null);
		rvoid.accept(er);
	}

	@Test
	public void testVisitVarDeclaration() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitFunctionApplication() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitAddExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitSubExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitMulExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitDivExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitGeExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitLeExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitGthExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitLthExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitOrExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitAndExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitNeExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitEquExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitModExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitUminusExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitNotExpression() {
		fail("Not yet implemented");
	}

	@Test
	public void testVisitExpressionInstruction() {
		fail("Not yet implemented");
	}

}
