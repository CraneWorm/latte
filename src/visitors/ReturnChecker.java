package visitors;

import java.util.Iterator;

import java_cup.runtime.Symbol;
import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteAddress;
import latte_program_tree.LatteInstruction;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;

public class ReturnChecker extends AbstractProgramNodeVisitor<Boolean> {

	private String context;
	
	private void parse_warning(String message, Symbol sym){
		errorReporter.report_warning(message, context, sym);
	}
	
	private void parse_error(String message, Symbol sym) {
		errorReporter.report_error(message, context, sym);		
	}
	
	@Override
	public Boolean visit(AssignInstruction o) {
		return false;
	}

	@Override
	public Boolean visit(DotExpression o) {
		return false;
	}

	@Override
	public Boolean visit(NewExpression o) {
		return false;
	}

	@Override
	public Boolean visit(BlockInstruction o) {
		Iterator<LatteInstruction> it = o.body.iterator();
		while(it.hasNext()){
			if(it.next().accept(this)){
				if (it.hasNext()){
					parse_warning("unreachable code", it.next().sym);
				}
				return true;
			}
		}
		return false;
	}

	@Override
	public Boolean visit(ClassDeclaration o) {
		boolean ret = true;
		for (FunctionDeclaration f: o.methods.values()){
			ret = ret && f.accept(this);
		}
		return ret;
	}

	@Override
	public Boolean visit(ConstExpression o) {
		return false;
	}

	@Override
	public Boolean visit(DecrementInstruction o) {
		return false;
	}

	@Override
	public Boolean visit(EmptyInstruction o) {
		return false;
	}

	@Override
	public Boolean visit(FunctionDeclaration o) {
		if (!o.return_type.name.equals("void") && !o.body.accept(this)){
			parse_error(
				String.format(
					"function %s must return a value of type: %s",
					o.function_name,
					o.return_type.name
				),
				o.sym
			);
			return false;
		}
		return true;
	}

	@Override
	public Boolean visit(IfInstruction o) {
		if (o.condition.is_const()){
			if ((Boolean)((ConstExpression)o.condition).value){
				return o.if_branch.accept(this);
			} else {
				return o.else_branch.accept(this);
			}
		} else {
			return o.if_branch.accept(this) && o.else_branch.accept(this);
		}		
	}

	@Override
	public Boolean visit(IncrementInstruction o) {
		return false;
	}

	@Override
	public Boolean visit(LatteProgram o) {
		Boolean ret = true;
		for (FunctionDeclaration fd: o.functions){
			if (!fd.return_type.name.equals("void")){				
				ret = ret && fd.accept(this);
			}
		}
		for (ClassDeclaration cd: o.classes){
			ret = ret && cd.accept(this);
		}
		return ret;
	}

	@Override
	public Boolean visit(LatteVar o) {
		return false;
	}

	@Override
	public Boolean visit(ReturnInstruction o) {
		return true;
	}

	@Override
	public Boolean visit(VarDeclaration o) {
		return false;
	}

	@Override
	public Boolean visit(WhileInstruction o) {
		if (o.condition.is_const()){
			if ((Boolean)((ConstExpression)o.condition).value){
				return o.loop.accept(this);
			} else {
				return false;
			}
		} else {
			return o.loop.accept(this);
		}		
	}

	@Override
	public Boolean visit(ForEach o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Boolean visit(FunctionApplication o) {
		if (o.function_name.equals("error")){
			return true;
		}
		return false;
	}

	@Override
	public Boolean visit(VarExpression o) {
		return false;
	}

	@Override
	public Boolean visit(AddExpression o) {
		return false;
	}

	@Override
	public Boolean visit(SubExpression o) {
		return false;
	}

	@Override
	public Boolean visit(MulExpression o) {
		return false;
	}

	@Override
	public Boolean visit(DivExpression o) {
		return false;
	}

	@Override
	public Boolean visit(GeExpression o) {
		return false;
	}

	@Override
	public Boolean visit(LeExpression o) {
		return false;
	}

	@Override
	public Boolean visit(GthExpression o) {
		return false;
	}

	@Override
	public Boolean visit(LthExpression o) {
		return false;
	}

	@Override
	public Boolean visit(OrExpression o) {
		return false;
	}

	@Override
	public Boolean visit(AndExpression o) {
		return false;
	}

	@Override
	public Boolean visit(NeExpression o) {
		return false;
	}

	@Override
	public Boolean visit(EquExpression o) {
		return false;
	}

	@Override
	public Boolean visit(LatteType o) {
		return false;
	}

	@Override
	public Boolean visit(ModExpression o) {
		return false;
	}

	@Override
	public Boolean visit(UminusExpression o) {
		return false;
	}

	@Override
	public Boolean visit(NotExpression o) {
		return false;
	}

	@Override
	public Boolean visit(ExpressionInstruction o) {
		return o.expression.accept(this);
	}

	@Override
	public Boolean visit(ArrayExpression arrayExpression) {
		return false;
	}

	@Override
	public Boolean visit(SelfExpression selfExpression) {
		return false;
	}
	
	@Override
	public Boolean visit(LatteAddress latteAddress) {
		return false;
	}

	@Override
	public Boolean visit(ArrayAllocation arrayAllocation) {
		return false;
	}


}
