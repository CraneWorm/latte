package visitors;

import helpers.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteAddress;
import latte_program_tree.LatteExpression;
import latte_program_tree.LatteInstruction;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ProgramStructNode;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;

public class JasminRewriter implements ProgramNodeVisitor<ProgramStructNode> {

	@SuppressWarnings("serial")
	public class ENV extends LinkedList<HashMap<String, LatteAddress>> {
		private int var_num = 0;
		public LatteAddress lookup(String ident) {
			Iterator<HashMap<String, LatteAddress>> it = iterator();
			while (it.hasNext()){
				LatteAddress ret = it.next().get(ident);
				if (ret != null){
					return ret;
				}
			}
			throw new Error("this never happened");// this will never happen
		}
		public int insert(LatteVar var){
			peek().put(var.ident, new LatteAddress(var, var_num));
			return var_num++;
		}
		public void reset(){
			var_num = 0;
			while(this.size() > 0)
			this.pop();
		}
	};
	
	private ENV env = new ENV();
	
	@Override
	public ProgramStructNode visit(AssignInstruction assignInstruction) {
		assignInstruction.rval = (LatteExpression) assignInstruction.rval.accept(this);
		assignInstruction.lval = (LatteExpression) assignInstruction.lval.accept(this);
		return assignInstruction;
	}

	@Override
	public ProgramStructNode visit(DotExpression dotExpression) {
		return null;
	}

	@Override
	public ProgramStructNode visit(NewExpression newExpression) {
		return null;
	}

	@Override
	public ProgramStructNode visit(BlockInstruction blockInstruction) {
		env.push(new HashMap<String, LatteAddress>());
		List<LatteInstruction> nbody = new ArrayList<LatteInstruction>();
		for (LatteInstruction st: blockInstruction.body){
			nbody.add((LatteInstruction) st.accept(this));			
		}
		blockInstruction.body = nbody;
		env.pop();
		return blockInstruction;
	}

	@Override
	public ProgramStructNode visit(ClassDeclaration classDeclaration) {
		return null;
	}

	@Override
	public ProgramStructNode visit(ConstExpression constExpression) {
		return constExpression;
	}

	@Override
	public ProgramStructNode visit(DecrementInstruction decrementInstruction) {
		decrementInstruction.var = (LatteExpression) decrementInstruction.var.accept(this);
		return decrementInstruction;
	}

	@Override
	public ProgramStructNode visit(EmptyInstruction emptyInstruction) {
		return emptyInstruction;
	}

	@Override
	public ProgramStructNode visit(FunctionDeclaration functionDeclaration) {
		env.reset();
		env.push(new HashMap<String, LatteAddress>());
		for (LatteVar var: functionDeclaration.argument_types){
			env.insert(var);
		}
		functionDeclaration.body.accept(this);
		functionDeclaration.function_name = 
				(functionDeclaration.function_name.equals("main")) ? 
						"MAIN" : 
						functionDeclaration.function_name;
		if (functionDeclaration.body.body.size() == 0){
			functionDeclaration.body.body.add(new ReturnInstruction(null, functionDeclaration.sym));
		}
		env.pop();
		return functionDeclaration;
	}

	@Override
	public ProgramStructNode visit(IfInstruction ifInstruction) {
		ifInstruction.condition = (LatteExpression) ifInstruction.condition.accept(this);
		ifInstruction.if_branch = (LatteInstruction) ifInstruction.if_branch.accept(this);
		ifInstruction.else_branch = (LatteInstruction) ifInstruction.else_branch.accept(this);
		return ifInstruction;
	}

	@Override
	public ProgramStructNode visit(IncrementInstruction incrementInstruction) {
		incrementInstruction.var = (LatteExpression) incrementInstruction.var.accept(this);
		return incrementInstruction;
	}

	@Override
	public ProgramStructNode visit(LatteProgram latteProgram) {
		List<FunctionDeclaration> nfun = new ArrayList<FunctionDeclaration>();
		
		for (FunctionDeclaration fd: latteProgram.functions){
			nfun.add((FunctionDeclaration) fd.accept(this));
		}
		
		latteProgram.functions = nfun;
		return latteProgram;
	}

	@Override
	public ProgramStructNode visit(LatteVar latteVar) {
		return env.lookup(latteVar.ident);
	}


	@Override
	public ProgramStructNode visit(ReturnInstruction returnInstruction) {
		if (returnInstruction.return_value != null){
			returnInstruction.return_value = 
					(LatteExpression) returnInstruction.return_value.accept(this);
		}
		return returnInstruction;
	}

	@Override
	public ProgramStructNode visit(VarDeclaration varDeclaration) {
		List<Pair<LatteVar, LatteExpression>> nvars = 
				new ArrayList<Pair<LatteVar,LatteExpression>>();
		
		for (Pair<LatteVar, LatteExpression> var: varDeclaration.vars){
			if (var.right != null){
				var.right = (LatteExpression) var.right.accept(this);
			}
			var.left.ident = ""+env.insert(var.left);
			nvars.add(var);
		}
		varDeclaration.vars = nvars;
		return varDeclaration;
	}

	@Override
	public ProgramStructNode visit(WhileInstruction whileInstruction) {
		whileInstruction.condition = 
				(LatteExpression) whileInstruction.condition.accept(this);
		whileInstruction.loop = (LatteInstruction) whileInstruction.loop.accept(this);
		return whileInstruction;
	}

	@Override
	public ProgramStructNode visit(ForEach forEach) {
		return null;
	}

	@Override
	public ProgramStructNode visit(FunctionApplication functionApplication) {
		List<LatteExpression> nargs = new ArrayList<LatteExpression>();
		for (LatteExpression exp: functionApplication.args){
			nargs.add((LatteExpression) exp.accept(this));
		}
		functionApplication.args = nargs;
		functionApplication.function_name = 
				(functionApplication.function_name.equals("main")) ?
						"MAIN" :
						functionApplication.function_name;
		return functionApplication;
	}

	@Override
	public ProgramStructNode visit(VarExpression varExpression) {
		return env.lookup(varExpression.ident);
	}

	@Override
	public ProgramStructNode visit(AddExpression addExpression) {
		addExpression.left = (LatteExpression) addExpression.left.accept(this);
		addExpression.right = (LatteExpression) addExpression.right.accept(this);
		return addExpression;
	}

	@Override
	public ProgramStructNode visit(SubExpression subExpression) {
		subExpression.left = (LatteExpression) subExpression.left.accept(this);
		subExpression.right = (LatteExpression) subExpression.right.accept(this);
		return subExpression;
	}

	@Override
	public ProgramStructNode visit(MulExpression mulExpression) {
		mulExpression.left = (LatteExpression) mulExpression.left.accept(this);
		mulExpression.right = (LatteExpression) mulExpression.right.accept(this);
		return mulExpression;
	}

	@Override
	public ProgramStructNode visit(DivExpression divExpression) {
		divExpression.left = (LatteExpression) divExpression.left.accept(this);
		divExpression.right = (LatteExpression) divExpression.right.accept(this);
		return divExpression;
	}

	@Override
	public ProgramStructNode visit(GeExpression geExpression) {
		geExpression.left = (LatteExpression) geExpression.left.accept(this);
		geExpression.right = (LatteExpression) geExpression.right.accept(this);
		return geExpression;
	}

	@Override
	public ProgramStructNode visit(LeExpression leExpression) {
		leExpression.left = (LatteExpression) leExpression.left.accept(this);
		leExpression.right = (LatteExpression) leExpression.right.accept(this);
		return leExpression;
	}

	@Override
	public ProgramStructNode visit(GthExpression gthExpression) {
		gthExpression.left = (LatteExpression) gthExpression.left.accept(this);
		gthExpression.right = (LatteExpression) gthExpression.right.accept(this);
		return gthExpression;
	}

	@Override
	public ProgramStructNode visit(LthExpression lthExpression) {
		lthExpression.left = (LatteExpression) lthExpression.left.accept(this);
		lthExpression.right = (LatteExpression) lthExpression.right.accept(this);
		return lthExpression;
	}

	@Override
	public ProgramStructNode visit(OrExpression orExpression) {
		orExpression.left = (LatteExpression) orExpression.left.accept(this);
		orExpression.right = (LatteExpression) orExpression.right.accept(this);
		return orExpression;
	}

	@Override
	public ProgramStructNode visit(AndExpression andExpression) {
		andExpression.left = (LatteExpression) andExpression.left.accept(this);
		andExpression.right = (LatteExpression) andExpression.right.accept(this);
		return andExpression;
	}

	@Override
	public ProgramStructNode visit(NeExpression neExpression) {
		neExpression.left = (LatteExpression) neExpression.left.accept(this);
		neExpression.right = (LatteExpression) neExpression.right.accept(this);
		return neExpression;
	}

	@Override
	public ProgramStructNode visit(EquExpression equExpression) {
		equExpression.left = (LatteExpression) equExpression.left.accept(this);
		equExpression.right = (LatteExpression) equExpression.right.accept(this);
		return equExpression;
	}

	@Override
	public ProgramStructNode visit(LatteType latteType) {
		return latteType;
	}

	@Override
	public ProgramStructNode visit(ModExpression modExpression) {
		modExpression.left = (LatteExpression) modExpression.left.accept(this);
		modExpression.right = (LatteExpression) modExpression.right.accept(this);
		return modExpression;
	}

	@Override
	public ProgramStructNode visit(UminusExpression uminusExpression) {
		uminusExpression.right = (LatteExpression) uminusExpression.right.accept(this);
		return uminusExpression;
	}

	@Override
	public ProgramStructNode visit(NotExpression notExpression) {
		notExpression.right = (LatteExpression) notExpression.right.accept(this);
		return notExpression;
	}

	@Override
	public ProgramStructNode visit(ExpressionInstruction expressionInstruction) {
		expressionInstruction.expression = (LatteExpression) expressionInstruction.expression.accept(this);
		return expressionInstruction;
	}

	@Override
	public ProgramStructNode visit(ArrayExpression arrayExpression) {
		return null;
	}

	@Override
	public ProgramStructNode visit(SelfExpression selfExpression) {
		return null;
	}

	@Override
	public ProgramStructNode visit(LatteAddress latteAddress) {
		return latteAddress;
	}

	@Override
	public ProgramStructNode visit(ArrayAllocation arrayAllocation) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
