package visitors;

import helpers.ErrorReporter;
import latte_program_tree.ProgramStructNode;

public class VisitorPipe {
	@SuppressWarnings("rawtypes")
	public static ErrorReporter _(ProgramStructNode program, AbstractProgramNodeVisitor...visitors){
		for (AbstractProgramNodeVisitor visitor: visitors){
			if (visitor.visit(program).errorReporter.error_count() > 0){
				return visitor.getErrorReporter();
			}
		}
		return new ErrorReporter();
	} 
}
