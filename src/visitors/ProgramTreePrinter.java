package visitors;



import helpers.Pair;

import java.util.Iterator;

import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteExpression;
import latte_program_tree.LatteInstruction;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;

public class ProgramTreePrinter extends AbstractProgramNodeVisitor<Void> {

	public int depth = 0;

	private void print(Object o) {
		System.out.print(o);
	}

	private void println(Object o) {
		String x = "";
		for (int i = depth; i > 0; i--)
			x += "    ";
		System.out.println(o);
		System.out.print(x);
	}

	@Override
	public Void visit(AssignInstruction o) {
		o.lval.accept(this);
		print(" = ");
		o.rval.accept(this);
		print(";");
		return null;
	}

	@Override
	public Void visit(BlockInstruction o) {
		depth++;
		print("{");
		for (LatteInstruction i : o.body) {
			println("");
			i.accept(this);
		}
		depth--;
		println("");
		print("}");
		return null;
	}

	@Override
	public Void visit(ClassDeclaration o) {
		print("class ");
		print(o.class_name);
		if (o.parent_class != null){
			print("extends ");
			print(o.parent_class);
		}
		print(" {");
		depth++;
		for (LatteVar var: o.attributes.values()){
			println("");
			var.accept(this);
			print(";");
		}
		for (FunctionDeclaration fun: o.methods.values()){
			println("");
			fun.accept(this);			
		}
		depth--;
		println("");
		print("}");
		println("");
		return null;
	}

	@Override
	public Void visit(ConstExpression o) {
		if (o.type.equals(new LatteType("string", null))){
			print('"'+((String)o.value)
					.replaceAll("([\n])", "\\\\n")
					.replaceAll("\\r","\\\\r")
					.replaceAll("\\t","\\\\t")
					.replaceAll("\\\"", "\\\\\"") + '"');
		} else {
			print(o.value);
		}
		return null;
	}

	@Override
	public Void visit(DecrementInstruction o) {
		o.var.accept(this);
		print("--;");
		return null;
	}

	@Override
	public Void visit(EmptyInstruction o) {
		print(";");
		return null;
	}

	@Override
	public Void visit(FunctionDeclaration o) {
		//println("");
		print(o.return_type.name);
		print(" ");
		print(o.function_name + "(");
		if (o.argument_types != null) {
			Iterator<LatteVar> args = o.argument_types.iterator();
			while(args.hasNext()){
				args.next().accept(this);
				if (args.hasNext()){
					print(", ");
				}			
			}
		}
		print(")");
		o.body.accept(this);
		println("");
		return null;
	}

	@Override
	public Void visit(IfInstruction o) {
		print("if (");
		o.condition.accept(this);
		print(")");
		depth++;
		println("");
		o.if_branch.accept(this);
		depth--;
		if (o.else_branch != null) {
			println("");
			print("else");
			depth++;
			println("");
			o.else_branch.accept(this);
			depth--;
		}
		return null;
	}

	@Override
	public Void visit(IncrementInstruction o) {
		o.var.accept(this);
		print("++;");
		return null;
	}

	@Override
	public Void visit(LatteProgram o) {
		for (ClassDeclaration cls : o.classes) {
			cls.accept(this);
		}
		for (FunctionDeclaration fd : o.functions) {
			fd.accept(this);
		}
		return null;
	}

	@Override
	public Void visit(VarExpression o) {
		if (o.object != null){
			o.object.accept(this);
			print(".");
		}
		print(o.ident);
		return null;
	}

	@Override
	public Void visit(LatteVar o) {
		o.type.accept(this);
		print(" ");
		print(o.ident);
		return null;
	}

	@Override
	public Void visit(ReturnInstruction o) {
		print("return ");
		if (o.return_value != null)
			o.return_value.accept(this);
		print(";");
		return null;
	}

	@Override
	public Void visit(VarDeclaration o) {
		o.type.accept(this);
		print(" ");
		Iterator<Pair<LatteVar, LatteExpression>> args = o.vars.iterator();
		while(args.hasNext()){
			Pair<LatteVar, LatteExpression> p = args.next();
			print(p.left.ident);
			if (p.right != null){
				print(" = ");
				p.right.accept(this);
			}
			if (args.hasNext()){
				print(", ");
			}			
		}
		print(";");
		return null;
	}

	@Override
	public Void visit(FunctionApplication o) {
		if (o.object != null){
			o.object.accept(this);
			print(".");
		}
		print(o.function_name + "(");
		if (o.args != null) {
			Iterator<LatteExpression> args = o.args.iterator();
			while(args.hasNext()){
				LatteExpression e = args.next();
				e.accept(this);
				if (args.hasNext()){
					print(", ");
				}			
			}
		}
		print(")");
		return null;
	}

	@Override
	public Void visit(WhileInstruction o) {
		print("while (");
		o.condition.accept(this);
		print(")");
		depth++;
		println("");
		o.loop.accept(this);
		depth--;
		return null;
	}

	@Override
	public Void visit(DotExpression o) {
		o.left.accept(this);
		print(".");
		o.right.accept(this);
		return null;
	}

	@Override
	public Void visit(NewExpression o) {
		print("new ");
		o.type.accept(this);
		if (o.expr != null){
			o.expr.accept(this);
		}
		return null;
	}

	@Override
	public Void visit(ForEach o) {
		print("for(");
		o.loop_var.accept(this);
		print(": ");
		print(o.array_expr);
		print(")");
		depth++;
		println("");
		o.loop_instruction.accept(this);
		depth--;
		return null;
	}

	@Override
	public Void visit(AddExpression o) {
		o.left.accept(this);
		print(" + "); 
		o.right.accept(this); 
		return null;
	}

	@Override
	public Void visit(SubExpression o) {
		o.left.accept(this);
		print(" - ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(MulExpression o) {
		o.left.accept(this);
		print(" * ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(DivExpression o) {
		o.left.accept(this);
		print(" / ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(GeExpression o) {
		o.left.accept(this);
		print(" >= ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(LeExpression o) {
		o.left.accept(this);
		print(" <= ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(GthExpression o) {
		o.left.accept(this);
		print(" > ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(LthExpression o) {
		o.left.accept(this);
		print(" < ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(OrExpression o) {
		o.left.accept(this);
		print(" || ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(AndExpression o) {
		o.left.accept(this);
		print(" && ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(NeExpression o) {
		o.left.accept(this);
		print(" != ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(EquExpression o) {
		o.left.accept(this);
		print(" == ");
		o.right.accept(this);
		return null;
		
	}

	@Override
	public Void visit(LatteType o) {
		print(o.name);
		return null;
	}

	@Override
	public Void visit(ModExpression o) {
		o.left.accept(this);
		print(" % ");
		o.right.accept(this);
		return null;
	}

	@Override
	public Void visit(UminusExpression o) {
		print("-");
		o.right.accept(this);
		return null;
	}

	@Override
	public Void visit(NotExpression o) {
		print("!");
		o.right.accept(this);
		return null;
	}

	@Override
	public Void visit(ExpressionInstruction o) {
		o.expression.accept(this);
		print(";");
		return null;
	}

	@Override
	public Void visit(ArrayExpression arrayExpression) {
		arrayExpression.array.accept(this);
		print("[");
		arrayExpression.index.accept(this);
		print("]");
		return null;
	}

	@Override
	public Void visit(SelfExpression selfExpression) {
		print("self");
		return null;
	}

	@Override
	public Void visit(ArrayAllocation arrayAllocation) {
		// TODO Auto-generated method stub
		return null;
	}
}
