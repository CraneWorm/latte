package visitors;

import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteAddress;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;



public interface ProgramNodeVisitor<T> {
	
	
	public T visit(AssignInstruction assignInstruction);

	public T visit(DotExpression dotExpression);
	
	public T visit(NewExpression newExpression);
	
	public T visit(BlockInstruction blockInstruction);

	public T visit(ClassDeclaration classDeclaration);

	public T visit(ConstExpression constExpression) ;

	public T visit(DecrementInstruction decrementInstruction);

	public T visit(EmptyInstruction emptyInstruction);

	public T visit(FunctionDeclaration functionDeclaration);

	public T visit(IfInstruction ifInstruction);

	public T visit(IncrementInstruction incrementInstruction);

	public T visit(LatteProgram latteProgram);
	
	public T visit(LatteVar latteVar);

	public T visit(ReturnInstruction returnInstruction);

	public T visit(VarDeclaration varDeclaration);

	public T visit(WhileInstruction whileInstruction);

	public T visit(ForEach forEach);

	public T visit(FunctionApplication functionApplication);

	public T visit(VarExpression varExpression);
	
	public T visit(AddExpression addExpression);
	
	public T visit(SubExpression subExpression);
	
	public T visit(MulExpression mulExpression);
	
	public T visit(DivExpression divExpression);
	
	public T visit(GeExpression geExpression);
	
	public T visit(LeExpression leExpression);
	
	public T visit(GthExpression gthExpression);
	
	public T visit(LthExpression lthExpression);
	
	public T visit(OrExpression orExpression);
	
	public T visit(AndExpression andExpression);
	
	public T visit(NeExpression neExpression);

	public T visit(EquExpression equExpression);

	public T visit(LatteType latteType);

	public T visit(ModExpression modExpression);

	public T visit(UminusExpression uminusExpression);

	public T visit(NotExpression notExpression);

	public T visit(ExpressionInstruction expressionInstruction);

	public T visit(ArrayExpression arrayExpression);

	public T visit(SelfExpression selfExpression);

	public T visit(LatteAddress latteAddress);

	public T visit(ArrayAllocation arrayAllocation);

}
