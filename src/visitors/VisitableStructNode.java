package visitors;


public interface VisitableStructNode {
	public <T> T accept(ProgramNodeVisitor<T> Visitor);
}
