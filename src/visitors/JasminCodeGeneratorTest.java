package visitors;

import helpers.ErrorReporter;
import helpers.FileFinder;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import latte_program_tree.LatteProgram;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import auto.Lexer;
import auto.Parser;

@RunWith(value = Parameterized.class)
public class JasminCodeGeneratorTest {
	private String basePath;
	
	public JasminCodeGeneratorTest(String basePath) {
		this.basePath = basePath;
	}
	
	@SuppressWarnings("rawtypes")
	@Parameters(name= "{index}:{0}")
	public static Collection data() {
		FileFinder sourceFinder = new FileFinder("*.lat");
        Path startingDir = Paths.get("lattests/good/");
		try {
			Files.walkFileTree(startingDir, sourceFinder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String[]> sourceArgs = new LinkedList<String[]>();
		List<String> results = sourceFinder.getResults();
		Collections.sort(results);
		for (String path: results){
			sourceArgs.add(new String[]{path});
		}
		return sourceArgs;
	}
	@Test
	public void testVisitLatteProgram() throws Exception {
		File f = new File(basePath);
		ErrorReporter err = new ErrorReporter();
		Parser parser = new Parser(new Lexer(new FileReader(f), err), err);
		LatteProgram P = (LatteProgram) parser.parse().value;
		String class_name = f.getName().split(".lat")[0];
		String source_name = class_name + ".j";
		JasminCodeGenerator jcg = new JasminCodeGenerator(source_name, class_name);
		if (P != null) {
			err = VisitorPipe._(P,
				new TypeChecker(),
				new ExpressionRewriter(),
				new ReturnChecker(),
				jcg				
			);
			String jasminPath = "lattests/good/" + source_name;
			String outPath = "lattests/good/";
			File j = new File(jasminPath);
			FileWriter writer = new FileWriter(j);
			writer.write(jcg.code());
			writer.flush();
			writer.close();
			java.lang.Runtime.getRuntime().exec(
					String.format(
							"java -jar lib/jasmin.jar -d %s %s",
							outPath,
							jasminPath
					)
			);
		}
		
	}

}
