package visitors;

import java.util.ListIterator;

import helpers.Pair;
import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteExpression;
import latte_program_tree.LatteInstruction;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;

public class ExpressionRewriter extends AbstractProgramNodeVisitor<LatteExpression> {

	@Override
	public LatteExpression visit(AssignInstruction o) {
		o.rval = o.rval.accept(this);
		return null;
	}

	@Override
	public LatteExpression visit(DotExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		return o;
	}

	@Override
	public LatteExpression visit(NewExpression o) {
		if (o.expr != null){
			o.expr = o.expr.accept(this);
		}
		return o;
	}

	@Override
	public LatteExpression visit(BlockInstruction o) {
		ListIterator<LatteInstruction> it = o.body.listIterator();
		while(it.hasNext()){
			LatteInstruction st = it.next();
			st.accept(this);
			if (st instanceof IfInstruction && ((IfInstruction) st).condition.is_const()){
				if ((boolean) ((ConstExpression)((IfInstruction) st).condition).value){
					it.set(((IfInstruction)st).if_branch);
				} else {
					it.set(((IfInstruction)st).else_branch);
				}
			}
		}
		return null;
	}

	@Override
	public LatteExpression visit(ClassDeclaration o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LatteExpression visit(ConstExpression o) {
		return o;
	}

	@Override
	public LatteExpression visit(DecrementInstruction o) {
		o.var = o.var.accept(this);
		return null;
	}

	@Override
	public LatteExpression visit(EmptyInstruction o) {
		return null;
	}

	@Override
	public LatteExpression visit(FunctionDeclaration o) {
		o.body.accept(this);
		return null;
	}

	@Override
	public LatteExpression visit(IfInstruction o) {
		o.condition = o.condition.accept(this);
		o.if_branch.accept(this);
		o.else_branch.accept(this);
		return null;
	}

	@Override
	public LatteExpression visit(IncrementInstruction o) {
		o.var = o.var.accept(this);
		return null;
	}

	@Override
	public LatteExpression visit(LatteProgram o) {
		for (FunctionDeclaration f: o.functions){
			f.body.accept(this);
		}
		return null;
	}

	@Override
	public LatteExpression visit(LatteVar o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LatteExpression visit(ReturnInstruction o) {
		if (o.return_value != null){
			o.return_value = o.return_value.accept(this);			
		}
		return null;
	}

	@Override
	public LatteExpression visit(VarDeclaration o) {
		for (Pair<LatteVar, LatteExpression> var: o.vars){
			if (var.right != null)
				var.right = var.right.accept(this);
		}
		return null;
	}

	@Override
	public LatteExpression visit(WhileInstruction o) {
		o.condition = o.condition.accept(this);
		o.loop.accept(this);
		return null;
	}

	@Override
	public LatteExpression visit(ForEach o) {
		o.array_expr = o.array_expr.accept(this);
		o.loop_instruction.accept(this);
		return null;
	}

	@Override
	public LatteExpression visit(FunctionApplication o) {
		if (o.object != null){
			o.object = o.object.accept(this);
		}
		for (int i = 0; i < o.args.size(); i++){
			o.args.set(i, o.args.get(i).accept(this));			
		}
		return o;
	}

	@Override
	public LatteExpression visit(VarExpression o) {
		if (o.object != null){
			o.object = o.object.accept(this);			
		}		
		return o;
	}

	@Override
	public LatteExpression visit(AddExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const() && ((ConstExpression)o.left).type.name.equals("int")){
			return new ConstExpression(
				"int",
				(Integer)((ConstExpression)o.left).value + (Integer)((ConstExpression)o.right).value,
				null);
		}
		if(o.left.is_const() && o.right.is_const() && ((ConstExpression)o.left).type.name.equals("string")){
			return new ConstExpression(
				"string",
				(String)((ConstExpression)o.left).value + (String)((ConstExpression)o.right).value,
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(SubExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"int",
				(Integer)((ConstExpression)o.left).value - (Integer)((ConstExpression)o.right).value,
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(MulExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"int",
				(Integer)((ConstExpression)o.left).value * (Integer)((ConstExpression)o.right).value,
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(DivExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"int",
				(Integer)((ConstExpression)o.left).value / (Integer)((ConstExpression)o.right).value,
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(GeExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"boolean",
				(Integer)((ConstExpression)o.left).value >= (Integer)((ConstExpression)o.right).value,
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(LeExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"boolean",
				(Integer)((ConstExpression)o.left).value <= (Integer)((ConstExpression)o.right).value,
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(GthExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"boolean",
				(Integer)((ConstExpression)o.left).value > (Integer)((ConstExpression)o.right).value,
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(LthExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"boolean",
				(Integer)((ConstExpression)o.left).value < (Integer)((ConstExpression)o.right).value,
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(OrExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"boolean",
				(Boolean)((ConstExpression)o.left).value || (Boolean)((ConstExpression)o.right).value,
				null);
		} else if (o.left.is_const()){
			if ((Boolean)((ConstExpression)o.left).value){
				return new ConstExpression(
					"boolean",
					new Boolean(true),
					null);
			} else {
				return o.right;
			}
		} else if (o.right.is_const()){
			if ((Boolean)((ConstExpression)o.right).value){
				return new ConstExpression(
					"boolean",
					new Boolean(true),
					null);
			} else {
				return o.left;
			}
		}		
		return o;
	}

	@Override
	public LatteExpression visit(AndExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"boolean",
				(Boolean)((ConstExpression)o.left).value && (Boolean)((ConstExpression)o.right).value,
				null);
		} else if (o.left.is_const()){ 
			if (!(Boolean)((ConstExpression)o.left).value){
				return new ConstExpression(
						"boolean",
						new Boolean(false),
						null);					
			} else {
				return o.right;
			}
		} else if (o.right.is_const()){
			if (!(Boolean)((ConstExpression)o.right).value){
				return new ConstExpression(
					"boolean",
					new Boolean(false),
					null);
			} else {
				return o.left;
			}
		}
		
		return o;
	}

	@Override
	public LatteExpression visit(NeExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"boolean",
				!((ConstExpression)o.left).value.equals(((ConstExpression)o.right).value),
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(EquExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"boolean",
				((ConstExpression)o.left).value.equals(((ConstExpression)o.right).value),
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(LatteType o) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LatteExpression visit(ModExpression o) {
		o.left = o.left.accept(this);
		o.right = o.right.accept(this);
		if(o.left.is_const() && o.right.is_const()){
			return new ConstExpression(
				"int",
				(Integer)((ConstExpression)o.left).value % (Integer)((ConstExpression)o.right).value,
				null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(UminusExpression o) {
		LatteExpression e = o.right.accept(this);
		if(e.is_const()){
			((ConstExpression)e).value = -(Integer)((ConstExpression)e).value;
			return e;
		}
		return o;
	}

	@Override
	public LatteExpression visit(NotExpression o) {
		LatteExpression e = o.right.accept(this);
		if(e.is_const()){
			((ConstExpression)e).value = !(Boolean)((ConstExpression)e).value;
			return e;
		}
		else if (e instanceof NotExpression){
			return ((NotExpression)e).right.accept(this);
		} else if (e instanceof AndExpression){
			return new OrExpression(
					new NotExpression(((AndExpression)e).left, null).accept(this),
					new NotExpression(((AndExpression)e).right, null).accept(this),
					null);
		} else if (e instanceof OrExpression){
			return new AndExpression(
					new NotExpression(((OrExpression)e).left, null).accept(this),
					new NotExpression(((OrExpression)e).right, null).accept(this),
					null);
		} else if (e instanceof EquExpression){
			return new NeExpression(
					((EquExpression)e).left.accept(this),
					((EquExpression)e).right.accept(this),
					null);
		} else if (e instanceof NeExpression){
			return new EquExpression(
					((NeExpression)e).left.accept(this),
					((NeExpression)e).right.accept(this),
					null);
		} else if (e instanceof GeExpression){
			return new LthExpression(
					((GeExpression)e).left.accept(this),
					((GeExpression)e).right.accept(this),
					null);
		} else if (e instanceof GthExpression){
			return new LeExpression(
					((GthExpression)e).left.accept(this),
					((GthExpression)e).right.accept(this),
					null);
		} else if (e instanceof LeExpression){
			return new GthExpression(
					((LeExpression)e).left.accept(this),
					((LeExpression)e).right.accept(this),
					null);
		} else if (e instanceof LthExpression){
			return new GeExpression(
					((LthExpression)e).left.accept(this),
					((LthExpression)e).right.accept(this),
					null);
		}
		return o;
	}

	@Override
	public LatteExpression visit(ExpressionInstruction o) {
		LatteExpression e = o.expression.accept(this);
		if(e.is_const()){
			o.expression = e;
			return e;
		}
		return null;
	}

	@Override
	public LatteExpression visit(ArrayExpression arrayExpression) {
		arrayExpression.array = arrayExpression.array.accept(this);
		arrayExpression.index = arrayExpression.index.accept(this);
		return arrayExpression;
	}

	@Override
	public LatteExpression visit(SelfExpression selfExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LatteExpression visit(ArrayAllocation arrayAllocation) {
		arrayAllocation.size_expression = arrayAllocation.size_expression.accept(this);
		return arrayAllocation;
	}

}
