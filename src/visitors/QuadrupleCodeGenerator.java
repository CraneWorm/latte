package visitors;

import static latteQuadCode.OP.*;

import static visitors.QuadrupleCodeGenerator.BoolExpCtx.AND_L;
import static visitors.QuadrupleCodeGenerator.BoolExpCtx.AND_R;
import static visitors.QuadrupleCodeGenerator.BoolExpCtx.IF_COND;
import static visitors.QuadrupleCodeGenerator.BoolExpCtx.NOT;
import static visitors.QuadrupleCodeGenerator.BoolExpCtx.OR_L;
import static visitors.QuadrupleCodeGenerator.BoolExpCtx.OR_R;
import static visitors.QuadrupleCodeGenerator.BoolExpCtx.VALUE;
import static visitors.QuadrupleCodeGenerator.BoolExpCtx.WHILE_COND;
import helpers.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;

import visitors.TypeChecker.Scope;
import latteQuadCode.CodeBlock;
import latteQuadCode.OP;
import latteQuadCode.QAddr;
import latteQuadCode.QFunc;
import latteQuadCode.QLabel;
import latteQuadCode.QName;
import latteQuadCode.QNode;
import latteQuadCode.QNum;
import latteQuadCode.QString;
import latteQuadCode.Quadruple;
import latteQuadCode.QuadrupleClass;
import latteQuadCode.QuadrupleCode;
import latteQuadCode.QuadrupleProgram;
import latte_program_tree.AddExpression;
import latte_program_tree.AndExpression;
import latte_program_tree.ArrayAllocation;
import latte_program_tree.ArrayExpression;
import latte_program_tree.AssignInstruction;
import latte_program_tree.BlockInstruction;
import latte_program_tree.ClassDeclaration;
import latte_program_tree.ConstExpression;
import latte_program_tree.DecrementInstruction;
import latte_program_tree.DivExpression;
import latte_program_tree.DotExpression;
import latte_program_tree.EmptyInstruction;
import latte_program_tree.EquExpression;
import latte_program_tree.ExpressionInstruction;
import latte_program_tree.ForEach;
import latte_program_tree.FunctionApplication;
import latte_program_tree.FunctionDeclaration;
import latte_program_tree.GeExpression;
import latte_program_tree.GthExpression;
import latte_program_tree.IfInstruction;
import latte_program_tree.IncrementInstruction;
import latte_program_tree.LatteExpression;
import latte_program_tree.LatteInstruction;
import latte_program_tree.LatteProgram;
import latte_program_tree.LatteType;
import latte_program_tree.LatteVar;
import latte_program_tree.LeExpression;
import latte_program_tree.LthExpression;
import latte_program_tree.ModExpression;
import latte_program_tree.MulExpression;
import latte_program_tree.NeExpression;
import latte_program_tree.NewExpression;
import latte_program_tree.NotExpression;
import latte_program_tree.OrExpression;
import latte_program_tree.ReturnInstruction;
import latte_program_tree.SelfExpression;
import latte_program_tree.SubExpression;
import latte_program_tree.UminusExpression;
import latte_program_tree.VarDeclaration;
import latte_program_tree.VarExpression;
import latte_program_tree.WhileInstruction;
public class QuadrupleCodeGenerator extends AbstractProgramNodeVisitor<QNode>{
	protected enum BoolExpCtx {
		AND_L, OR_L, AND_R, OR_R, VALUE, IF_COND, WHILE_COND, NOT
	}
	@SuppressWarnings("serial")
	private LinkedList<BoolExpCtx> bctx = new LinkedList<BoolExpCtx>(){{ push(VALUE); }};
	private LinkedList<QNode> jump_true = new LinkedList<QNode>();
	private LinkedList<QNode> jump_false = new LinkedList<QNode>();
	private HashMap<String, QuadrupleClass> classes;
	private HashMap<String, QuadrupleCode> functions;
	private List<Quadruple> quads;
	public LinkedList<QuadrupleCode> functions(){ return new LinkedList<QuadrupleCode>(functions.values()); }
	public LinkedList<QuadrupleClass> classes(){ return new LinkedList<QuadrupleClass>(env.classes.values()); }
	private HashMap<String, ClassDeclaration> class_declarations;
	private LinkedHashSet <QString> strings = new LinkedHashSet<QString>();
	public QuadrupleProgram program(){
		return new QuadrupleProgram(classes(), functions(), new LinkedList<QString>(strings));
	}
	@SuppressWarnings("serial")
	public class ENV extends LinkedList<HashMap<String, QAddr>> {
		private HashMap<String, QAddr> locals;
		private LinkedList<QAddr> args;
		private LinkedList<Integer> blocks_decl_size;
		private HashMap<String, QuadrupleClass> classes;
		public QAddr lookup(String ident) {
			Iterator<HashMap<String, QAddr>> it = iterator();
			while (it.hasNext()){
				QAddr ret = it.next().get(ident);
				if (ret != null){
					return ret;
				}
			}
			throw new Error("this never happened");// this will never happen
		}
		public QAddr arg(String id){
			QAddr arg = new QAddr(id);
			args.add(arg);
			peek().put(id, arg);
			return arg;
		}
		public QAddr local(String var){
			QAddr addr = new QAddr(var);
			if (!locals.containsKey(addr.id())){
				peek().put(var, addr);
				locals.put(addr.id(), addr);
				return addr;
			}
			addr = locals.get(addr.id());
			peek().put(var, addr);
			return addr;
		}
		public QAddr tmp() {
			QAddr addr = new QAddr();
			if (!locals.containsKey(addr.id())){
				locals.put(addr.id(), addr);
				return addr;
			}
			return locals.get(addr.id());
		}
		public void reset(){
			locals = new HashMap<String, QAddr>();
			args = new LinkedList<QAddr>();
			blocks_decl_size = new LinkedList<Integer>();
			while(this.size() > 0)
			this.pop();
			env.clear();
			QAddr.resetc();
		}
		public void push(HashMap<String, QAddr> map){
			blocks_decl_size.push(QAddr.c());
			super.push(map);
		}
		public HashMap<String, QAddr> pop(){
			peek().clear();
			// all slots for variables declared in inner block can be reused
			// we reset the numeration
			QAddr.resetc(blocks_decl_size.pop());
			return super.pop();
		}
		public void classes(List<ClassDeclaration> program_classes){
			HashMap<String, ClassDeclaration> classes_map = 
					new HashMap<String, ClassDeclaration>();
			classes = new LinkedHashMap<String, QuadrupleClass>();
			for (ClassDeclaration cldn: program_classes){
				classes_map.put(cldn.class_name, cldn);
			}
			LinkedList<ClassDeclaration> inheritance_stack = new LinkedList<ClassDeclaration>();
			QuadrupleClass declared_ancestor;
			for (ClassDeclaration class_decl: classes_map.values()){
				declared_ancestor = null;
				if (!classes.containsKey(class_decl.class_name)){
					inheritance_stack.push(class_decl);
					while(class_decl.parent_class != null && 
							(declared_ancestor = classes.get(class_decl.parent_class)) == null){
						inheritance_stack.push(class_decl = classes_map.get(class_decl.parent_class));
					}
					if (declared_ancestor == null){
						declared_ancestor = new QuadrupleClass();
					}
					for(;!inheritance_stack.isEmpty();){
						ClassDeclaration ancestor = inheritance_stack.pop();
						declared_ancestor = declared_ancestor.extend(ancestor);
						classes.put(ancestor.class_name, declared_ancestor);
					}
				}
			}			
		}
	};
	
	private ENV env = new ENV();
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for (String fname: functions.keySet()){
			QuadrupleCode f = functions.get(fname);
			sb.append(fname + ":\n");
			for (CodeBlock b: f.blocks()){
				if (b.jumps().size() > 0){
					sb.append("." + b.label() + ":\n");
				}
				for (Quadruple q: b.quads()){					
					sb.append("\t" + q.toString() + "\n");
				}
			}
		}
		return sb.toString();
	}
	
	@Override
	public QNode visit(AssignInstruction assignInstruction) {
		if (assignInstruction.lval instanceof ArrayExpression){
			ArrayExpression arr_exp = (ArrayExpression)assignInstruction.lval;
			QNode	array 	= arr_exp.array.accept(this),
					index	= arr_exp.index.accept(this),
					value 	= assignInstruction.rval.accept(this);
			quads.add(new Quadruple(put_i, array, index, value));
			return null;
		} else if (assignInstruction.lval instanceof VarExpression 
				&& ((VarExpression)assignInstruction.lval).object != null){
			VarExpression var_expr = (VarExpression)assignInstruction.lval;
			QNode 	
			object	= var_expr.object.accept(this),
			value 	= assignInstruction.rval.accept(this);
			VarExpression lval = (VarExpression)assignInstruction.lval;
			String ident = lval.ident;
			String ctype = lval.object.type.name;
			ClassDeclaration cld = class_declarations.get(ctype);
			while (!cld.attributes.containsKey(ident)){
				ctype = (cld = class_declarations.get(cld.parent_class)).class_name;
			}
			quads.add(new Quadruple(set_m, object, new QName(ctype + "::" + ident), value));
			return null;
		} else {
			QNode 	left 	= assignInstruction.lval.accept(this),
					right 	= assignInstruction.rval.accept(this);
			quads.add(new Quadruple(mov, left, right));
			return left;
		}
	}

	@Override
	public QNode visit(DotExpression dotExpression) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public QNode visit(NewExpression newExpression) {
		QNode ref = env.tmp();
		quads.add(new Quadruple(_new, ref, new QName(newExpression.type.name)));
		quads.add(new Quadruple(arg, ref));
		quads.add(new Quadruple(call_method, env.tmp(),
				new QFunc(newExpression.type.name + "__" + newExpression.type.name), new QNum(1)));
		return ref;
	}

	@Override
	public QNode visit(BlockInstruction blockInstruction) {
		env.push(new HashMap<String, QAddr>());
		for (LatteInstruction i: blockInstruction.body){
			i.accept(this);
		}
		env.pop();
		return null;
	}

	@Override
	public QNode visit(ClassDeclaration classDeclaration) {
		
		return null;
	}

	@Override
	public QNode visit(ConstExpression constExpression) {
		QNode ret = null;
		if (constExpression.type.name.equals("int")){
			ret = new QNum((int)constExpression.value);
		} else if(constExpression.type.name.equals("boolean")){
			ret = new QNum((boolean)constExpression.value ? 1:0);
		} else if (constExpression.type.name.equals("string")){
			ret = QString.string((String)constExpression.value);
			strings.add((QString) ret);
		} else {
			ret = new QNum(0);
		}
		return ret;
	}

	@Override
	public QNode visit(DecrementInstruction decrementInstruction) {
		LatteExpression minusOne = new SubExpression(decrementInstruction.var,
				new ConstExpression("int", new Integer(1), null),
				decrementInstruction.sym
		);
		minusOne.type = new LatteType("int", null);
		return new AssignInstruction(
				decrementInstruction.var,
				minusOne, 
				decrementInstruction.sym).accept(this);
	}

	@Override
	public QNode visit(EmptyInstruction emptyInstruction) {
		return null;
	}

	@Override
	public QNode visit(FunctionDeclaration functionDeclaration) {
		env.push(new HashMap<String, QAddr>());
		for (LatteVar var: functionDeclaration.argument_types){			
			env.arg(var.ident);
		}
		functionDeclaration.body.accept(this);
		env.pop();
		return null;
	}

	@Override
	public QNode visit(IfInstruction ifInstruction) {
		QLabel 	ltrue = QLabel.fresh(),
				lfalse = QLabel.fresh(),
				lend;
		ReturnChecker rc = new ReturnChecker();
		boolean returns = ifInstruction.if_branch.accept(rc),
				empty_else = (ifInstruction.else_branch instanceof EmptyInstruction);
		jump_true.push(ltrue);
		jump_false.push(lfalse);
		if (!empty_else){
			lend = QLabel.fresh();
		} else {
			lend = lfalse;
		}
		bctx.push(IF_COND);
		ifInstruction.condition.accept(this);
		bctx.pop();
		jump_true.pop();
		jump_false.pop();
		
		quads.add(new Quadruple(label, ltrue));
		ifInstruction.if_branch.accept(this);
		if (!empty_else){
			if (!returns){
				quads.add(new Quadruple(jmp, lend));
			}
			quads.add(new Quadruple(label, lfalse));
			ifInstruction.else_branch.accept(this);
			if (!returns){
				quads.add(new Quadruple(label, lend));
			}
		} else {
			quads.add(new Quadruple(label, lfalse));
		}
		
		return null;
	}

	@Override
	public QNode visit(IncrementInstruction incrementInstruction) {
		LatteExpression plusOne = new AddExpression(incrementInstruction.var,
				new ConstExpression("int", new Integer(1), null),
				incrementInstruction.sym
		);
		plusOne.type = new LatteType("int", null);
		return new AssignInstruction(
				incrementInstruction.var,
				plusOne, 
				incrementInstruction.sym).accept(this);		
	}

	@Override
	public QNode visit(LatteProgram latteProgram) {
		class_declarations = new HashMap<String, ClassDeclaration>();
		for (ClassDeclaration c: latteProgram.classes){
			class_declarations.put(c.class_name, c);
			c.methods.put(c.class_name, createConstructor(c));
		}
		functions = new HashMap<String, QuadrupleCode>();
		classes = new HashMap<String, QuadrupleClass>();
		env = new ENV();
		env.classes(latteProgram.classes);
		for (FunctionDeclaration f: latteProgram.functions){
			quads = new LinkedList<Quadruple>();
			env.reset();
			f.accept(this);
			QuadrupleCode qfunc = new QuadrupleCode(quads);
			qfunc.locals(env.locals);
			qfunc.args(env.args);
			qfunc.name(f.function_name);
			functions.put(f.function_name, qfunc);			
		}
		for (ClassDeclaration c: latteProgram.classes){
			for (FunctionDeclaration method: c.methods.values()){
				quads = new LinkedList<Quadruple>();
				env.reset();
				env.push(new HashMap<String, QAddr>());
				env.arg("self");
				method.accept(this);
				env.pop();
				QuadrupleCode qfunc = new QuadrupleCode(quads);
				qfunc.locals(env.locals);
				qfunc.args(env.args);
				qfunc.name(c.class_name+ "__" + method.function_name);
				functions.put(c.class_name+ "__" + method.function_name, qfunc);
			}
		}
		return null;
	}
	
	private FunctionDeclaration createConstructor(ClassDeclaration class_declaration){
		BlockInstruction body = new BlockInstruction(null);
		
		if (class_declarations.containsKey(class_declaration.parent_class)){
			FunctionApplication parent_ctor_call = new FunctionApplication(
					class_declaration.parent_class,
					new LinkedList<LatteExpression>(),
					null);
			parent_ctor_call.object = new SelfExpression(null);
			parent_ctor_call.object.type = new LatteType(class_declaration.class_name, null);
			body.add(new ExpressionInstruction(parent_ctor_call, null));
		}
		
		for (LatteVar attribute: class_declaration.attributes.values()){
			VarExpression attribute_expression = new VarExpression(attribute.ident, null);
			attribute_expression.object = new SelfExpression(null);
			attribute_expression.object.type = new LatteType(class_declaration.class_name, null);
			Object value;
			
			if (attribute.type.name.equals("string")) value = "";
			else if (attribute.type.name.equals("boolean")) value = false;
			else value = 0;
			
			ConstExpression attribute_initializer = new ConstExpression(attribute.type.name,
					value, null);
			
			AssignInstruction initialize_attr = new AssignInstruction(
					attribute_expression,
					attribute_initializer, null);
			
			body.add(initialize_attr);
		}
		
		FunctionDeclaration constructor = new FunctionDeclaration(
				new LatteType("void", null),
				class_declaration.class_name,
				new LinkedList<LatteVar>(),
				body, null);
		return constructor;
	}
	
	@Override
	public QNode visit(LatteVar latteVar) {
		switch (bctx.peek()){
			case AND_L: case AND_R: case IF_COND: case NOT:
				case OR_L: case OR_R: case WHILE_COND:
				
				return new EquExpression(
						new VarExpression(latteVar.ident, null),
						new ConstExpression("boolean", true, null), 
						null).accept(this);
			case VALUE:
				return env.local(latteVar.ident);
		}
		return env.local(latteVar.ident);
	}

	@Override
	public QNode visit(ReturnInstruction returnInstruction) {
		if (returnInstruction.return_value != null){
			QNode rval = returnInstruction.return_value.accept(this);
			quads.add(new Quadruple(ret, rval));
		} else {
			quads.add(new Quadruple(ret));
		}
		return null;
	}

	@Override
	public QNode visit(VarDeclaration varDeclaration) {
		for (Pair<LatteVar, LatteExpression> p: varDeclaration.vars){
			if (p.right != null){
				QNode rval = p.right.accept(this);
				QAddr loc = env.local(p.left.ident);
				quads.add(new Quadruple(mov, loc, rval));
			} else {
				QAddr loc = env.local(p.left.ident);
				if (p.left.type.name.equals("string")){
					QString es = QString.string("");
					quads.add(new Quadruple(mov, loc, es));
					strings.add((QString) es);
				} else {
					quads.add(new Quadruple(mov, loc, new QNum(0)));
				}
			}
		}
		return null;
	}

	@Override
	public QNode visit(WhileInstruction whileInstruction) {
		QLabel 	lcond = QLabel.fresh(),
				lbody = QLabel.fresh(),
				lend = QLabel.fresh();
		quads.add(new Quadruple(jmp, lcond));
		quads.add(new Quadruple(label, lbody));
		whileInstruction.loop.accept(this);
		jump_true.push(lbody);
		jump_false.push(lend);
		quads.add(new Quadruple(label, lcond));
		bctx.push(WHILE_COND);
		whileInstruction.condition.accept(this);
		bctx.pop();
		jump_true.pop();
		jump_false.pop();
		quads.add(new Quadruple(label, lend));
		return null;
	}

	@Override
	public QNode visit(ForEach forEach) {
		QAddr 	ctr = env.tmp(),
				ln 	= env.tmp(),
				arr = (QAddr) forEach.array_expr.accept(this);
		
		quads.add(new Quadruple(len, ln, arr));
		quads.add(new Quadruple(mov, ctr, new QNum(0)));
		
		QLabel 	fcond = QLabel.fresh(),
				fbody = QLabel.fresh(),
				fend  = QLabel.fresh();
		
		quads.add(new Quadruple(jmp, fcond));
		quads.add(new Quadruple(label, fbody));
		env.push(new HashMap<String, QAddr>());
		QAddr 	x	= env.local(forEach.loop_var.ident);
		quads.add(new Quadruple(get_i, x, arr, ctr));
		forEach.loop_instruction.accept(this);
		env.pop();
		quads.add(new Quadruple(inc, ctr));
		quads.add(new Quadruple(label, fcond));
		quads.add(new Quadruple(cmp, ctr, ln));
		quads.add(new Quadruple(jge, fend));
		quads.add(new Quadruple(jmp, fbody));
		quads.add(new Quadruple(label, fend));
		return null;
	}

	@Override
	public QNode visit(FunctionApplication functionApplication) {
		if (bctx.peek() != VALUE){
			QNode ret =  new EquExpression(
					functionApplication,
					new ConstExpression("boolean", true, null),
					null).accept(this);
			return ret;
		}
			
		QAddr ret = env.tmp();
		if (functionApplication.object == null){
			List<LatteExpression> rev_args = new ArrayList<LatteExpression>(functionApplication.args);
			Collections.reverse(rev_args);
			for (LatteExpression a: rev_args){
				bctx.push(VALUE);
				QNode qarg = a.accept(this);
				bctx.pop();
				quads.add(new Quadruple(arg, qarg));
			}
			quads.add(new Quadruple(call, ret, new QFunc(functionApplication.function_name), 
					new QNum(functionApplication.args.size())));
		} else {
			QNode self = functionApplication.object.accept(this);
			List<LatteExpression> rev_args = new ArrayList<LatteExpression>(functionApplication.args);
			Collections.reverse(rev_args);
			for (LatteExpression a: rev_args){
				bctx.push(VALUE);
				QNode qarg = a.accept(this);
				bctx.pop();
				quads.add(new Quadruple(arg, qarg));
			}
			quads.add(new Quadruple(arg, self));
			quads.add(new Quadruple(call_method, ret, new QFunc(
					functionApplication.object.type.name + "__" +
					functionApplication.function_name), 
					new QNum(functionApplication.args.size()+1)));
		}
		return ret;
	}

	@Override
	public QNode visit(VarExpression varExpression) {
		if (varExpression.object != null){
			QNode parent = varExpression.object.accept(this);
			QAddr ret = env.tmp();
			if (varExpression.object.type.name.endsWith("[]")){
				quads.add(new Quadruple(len, ret, parent));
				return ret;
			}
			String ident = varExpression.ident;
			String ctype = varExpression.object.type.name;
			ClassDeclaration cld = class_declarations.get(ctype);
			while (!cld.attributes.containsKey(ident)){
				ctype = (cld = class_declarations.get(cld.parent_class)).class_name;
			}
			quads.add(new Quadruple(get_m, ret, parent, new QName(ctype + "::" + ident)));
			return ret;
		}
		switch (bctx.peek()){
			case AND_L: case AND_R: case IF_COND: case NOT:
			case OR_L: case OR_R: case WHILE_COND:
				
				return new EquExpression(
					varExpression,
					new ConstExpression("boolean", true, null), 
					null).accept(this);
			case VALUE:
				return env.lookup(varExpression.ident);
			default:
				break;
		}
		return null;
	}

	@Override
	public QNode visit(AddExpression addExpression) {
		QNode	left = addExpression.left.accept(this),
				right = addExpression.right.accept(this),
				ret = env.tmp();
		latteQuadCode.OP op = (addExpression.type.name.equals("string")) ? cat : add;
		quads.add(new Quadruple(op, ret, left, right));
		return ret;
	}

	@Override
	public QNode visit(SubExpression subExpression) {
		QNode 	left = subExpression.left.accept(this),
				right = subExpression.right.accept(this),
				ret = env.tmp();
		quads.add(new Quadruple(sub, ret, left, right));
		return ret;
	}

	@Override
	public QNode visit(MulExpression mulExpression) {
		QNode 	left = mulExpression.left.accept(this),
				right = mulExpression.right.accept(this),
				ret = env.tmp();
		quads.add(new Quadruple(imul, ret, left, right));
		return ret;
	}

	@Override
	public QNode visit(DivExpression divExpression) {
		QNode 	left = divExpression.left.accept(this),
				right = divExpression.right.accept(this),
				ret = env.tmp();
		quads.add(new Quadruple(div, ret, left, right));
		return ret;
	}
	
	private OP neg_jmp(OP op){
		switch(op){
			case je: return jne;
			case jne: return je;
			case jge: return jl;
			case jg: return jle;
			case jle: return jg;
			case jl: return jge;
			default: return null;
		}
	}	
	
	private QNode valueOrJump(OP j, LatteExpression left, LatteExpression right){
		/* 
		 * checks context of current expression
		 * returns jump or value as needed
		 * also, tries to be smart with
		 * generating jumps
		*/
		if (bctx.peek() == NOT){
			bctx.pop();
			j = neg_jmp(j);
		}
		bctx.push(VALUE);
		QNode 	qleft = left.accept(this),
				qright = right.accept(this);
		bctx.pop();
		quads.add(new Quadruple(cmp, qleft, qright));
		switch (bctx.peek()){
			case AND_L: case AND_R: case IF_COND:
				// jump to false on false
				j = neg_jmp(j);
				quads.add(new Quadruple(j, jump_false.peek()));
				break;
			case OR_L: case OR_R: case WHILE_COND:
				// jump to true on true
				quads.add(new Quadruple(j, jump_true.peek()));
				break;
			case VALUE:
				QLabel lend = QLabel.fresh(), lfalse = QLabel.fresh();
				QAddr value = env.tmp();
				// yes, it's possible we revert the jump to it's original form
				j = neg_jmp(j);
				// if the condition fails we jump to false
				quads.add(new Quadruple(j, lfalse));
				// else, we put 1...
				quads.add(new Quadruple(mov, value, new QNum(1)));
				// and jump over lfalse code
				quads.add(new Quadruple(jmp, lend));
				quads.add(new Quadruple(label, lfalse));
				quads.add(new Quadruple(mov, value, new QNum(0)));
				quads.add(new Quadruple(label, lend));
				// in this case we are interested in the value
				return value;
			default:
				break;
		}		
		return null;
	}
	
	@Override
	public QNode visit(GeExpression geExpression) {
		return valueOrJump(jge, geExpression.left, geExpression.right);
	}

	@Override
	public QNode visit(LeExpression leExpression) {
		return valueOrJump(jle, leExpression.left, leExpression.right);
	}

	@Override
	public QNode visit(GthExpression gthExpression) {
		return valueOrJump(jg, gthExpression.left, gthExpression.right);
	}

	@Override
	public QNode visit(LthExpression lthExpression) {
		return valueOrJump(jl, lthExpression.left, lthExpression.right);
	}

	@Override
	public QNode visit(OrExpression orExpression) {
		boolean val = bctx.peek() == VALUE;
		QLabel 	jump = QLabel.fresh(),
				vtrue = null,
				vfalse = null;
		// if left branch evaluates to false early we need to jump into right
		jump_false.push(jump);
		// left branch needs to know the proper context
		bctx.push(OR_L);
		if (val){
			vtrue = QLabel.fresh();
			jump_true.push(vtrue);
		}
		orExpression.left.accept(this);
		// remember to pop the label! 
		jump_false.pop();
		if (val){
			vfalse = QLabel.fresh();
			jump_false.push(vfalse);
		}
		quads.add(new Quadruple(label, jump));		
		bctx.pop();
		bctx.push(OR_R);
		orExpression.right.accept(this);
		bctx.pop();
		if (val){
			jump_true.pop();
			jump_false.pop();
			QAddr ret = env.tmp();
			QLabel vend = QLabel.fresh();
			quads.add(new Quadruple(label, vfalse));
			quads.add(new Quadruple(mov, ret, new QNum(0)));
			quads.add(new Quadruple(jmp, vend));
			quads.add(new Quadruple(label, vtrue));
			quads.add(new Quadruple(mov, ret, new QNum(1)));
			quads.add(new Quadruple(label, vend));
			return ret;
		}
		quads.add(new Quadruple(jmp, jump_false.peek()));
		return null;
	}

	@Override
	public QNode visit(AndExpression andExpression) {
		boolean val = bctx.peek() == VALUE;
		QLabel 	jump = QLabel.fresh(),
				vtrue = null,
				vfalse = null; 
		// if left branch evaluates to true we still need to jump into right
		jump_true.push(jump);
		// left branch needs to know the proper context
		bctx.push(AND_L);
		if (val){
			vfalse = QLabel.fresh();
			jump_false.push(vfalse);
		}
		andExpression.left.accept(this);
		// remember to pop the appropriate label!
		jump_true.pop();
		
		quads.add(new Quadruple(label, jump));
		bctx.pop();
		bctx.push(AND_R);
		if (val){
			vtrue = QLabel.fresh();
			jump_true.push(vtrue);
		}
		andExpression.right.accept(this);
		bctx.pop();
		if (val){
			jump_false.pop();
			jump_true.pop();
			QAddr ret = env.tmp();
			QLabel vend = QLabel.fresh();
			quads.add(new Quadruple(label, vtrue));
			quads.add(new Quadruple(mov, ret, new QNum(1)));
			quads.add(new Quadruple(jmp, vend));
			quads.add(new Quadruple(label, vfalse));
			quads.add(new Quadruple(mov, ret, new QNum(0)));
			quads.add(new Quadruple(label, vend));
			return ret;
		}
		quads.add(new Quadruple(jmp, jump_true.peek()));
		return null;
	}

	@Override
	public QNode visit(NeExpression neExpression) {
		return valueOrJump(jne, neExpression.left, neExpression.right);
	}

	@Override
	public QNode visit(EquExpression equExpression) {
		return valueOrJump(je, equExpression.left, equExpression.right);
	}

	@Override
	public QNode visit(LatteType latteType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public QNode visit(ModExpression modExpression) {
		QNode 	left = modExpression.left.accept(this),
				right = modExpression.right.accept(this),
				ret = env.tmp();
		quads.add(new Quadruple(mod, ret, left, right));
		return ret;
	}

	@Override
	public QNode visit(UminusExpression uminusExpression) {
		QNode 	right = uminusExpression.right.accept(this),
				ret = env.tmp();
		quads.add(new Quadruple(sub, ret, new QNum(0), right));
		return ret;
	}

	@Override
	public QNode visit(NotExpression notExpression) {
		bctx.push(NOT);
		return notExpression.right.accept(this);		
	}
	
	@Override
	public QNode visit(ExpressionInstruction expressionInstruction) {
		return expressionInstruction.expression.accept(this);
	}

	@Override
	public QNode visit(ArrayExpression arrayExpression) {
		if (bctx.peek() != VALUE){
			return new EquExpression(
					arrayExpression,
					new ConstExpression("boolean", true, null), 
					null).accept(this);
			
		} else {
			QNode array = arrayExpression.array.accept(this);
			QNode index = arrayExpression.index.accept(this);
			QAddr addr = env.tmp();
			quads.add(new Quadruple(OP.get_i, addr, array, index));
			return addr;
		}
	}

	@Override
	public QNode visit(SelfExpression selfExpression) {
		QAddr ret = env.lookup("self");		
		return ret;
	}

	@Override
	public QNode visit(ArrayAllocation arrayAllocation) {
		QAddr array = env.tmp();
		QNode size = arrayAllocation.size_expression.accept(this);
		quads.add(new Quadruple(a_alloc, array, size));
		return array;
	}

}
