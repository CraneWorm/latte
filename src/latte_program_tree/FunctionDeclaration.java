package latte_program_tree;

import java.util.ArrayList;
import java.util.List;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class FunctionDeclaration extends ProgramStructNode {
	public LatteType return_type;
	public String function_name;
	public LatteFunctionType function_type;
	public List<LatteVar> argument_types;
	public BlockInstruction body;
	
	public FunctionDeclaration(LatteType return_type, String function_name,
			List<LatteVar> argument_types, BlockInstruction body, Symbol s) {
		super(s);
		this.return_type = return_type;
		this.function_name = function_name;
		this.argument_types = argument_types;
		List<LatteType> args_types = new ArrayList<LatteType>();
		if (argument_types != null){
			for (LatteVar v: argument_types){args_types.add(v.type);}
		}
		this.function_type = new LatteFunctionType(return_type, args_types);
		this.body = body;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
