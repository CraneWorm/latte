package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class ExpressionInstruction extends LatteInstruction {
	public LatteExpression expression;
	public ExpressionInstruction(LatteExpression expression, Symbol s) {
		super(s);
		this.expression = expression;
	}
	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
