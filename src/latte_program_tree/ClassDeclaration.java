package latte_program_tree;

import java.util.LinkedHashMap;

import java_cup.runtime.Symbol;
import visitors.ProgramNodeVisitor;

public class ClassDeclaration extends ProgramStructNode {
	public String class_name;
	public String parent_class;
	public LinkedHashMap<String, FunctionDeclaration> methods;
	public LinkedHashMap<String, LatteVar> attributes;

	public ClassDeclaration(String class_name, Symbol s) {
		super(s);
		this.class_name = class_name;
		methods = new LinkedHashMap<String, FunctionDeclaration>();
		attributes = new LinkedHashMap<String, LatteVar>();
	}
	public ClassDeclaration method(FunctionDeclaration fun){
		methods.put(fun.function_name, fun);
		return this;
	}
	public ClassDeclaration attribute(LatteVar var){
		attributes.put(var.ident, var);
		return this;
	}
	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
