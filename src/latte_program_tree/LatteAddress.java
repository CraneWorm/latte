package latte_program_tree;

import visitors.ProgramNodeVisitor;

public class LatteAddress extends VarExpression {
	
	public int address;
	public LatteType type;
	public LatteAddress(LatteVar latteVar, int address){
		super(latteVar.ident, latteVar.sym);
		this.type = latteVar.type;
		this.address = address;
	}
	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
