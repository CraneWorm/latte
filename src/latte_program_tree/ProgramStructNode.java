package latte_program_tree;

import visitors.VisitableStructNode;
import java_cup.runtime.Symbol;

public abstract class ProgramStructNode implements VisitableStructNode{
	public Symbol sym;
	public ProgramStructNode(Symbol s) {
		this.sym = s;
	}
	

}
