package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class ConstExpression extends LatteExpression {
	public Object value;
	public LatteType type;
	public ConstExpression(String type, Object value, Symbol s) {
		super(s);
		this.type = new LatteType(type, s);
		this.value = value;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
	public boolean is_const(){
		return true;
	}

}
