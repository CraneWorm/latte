package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class MulExpression extends LatteExpression {
	public LatteExpression left;
	public LatteExpression right;
	
	public MulExpression(LatteExpression e1, LatteExpression e2, Symbol s) {
		super(s);
		left = e1;
		right = e2;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
