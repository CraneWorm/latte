package latte_program_tree;

import java.util.ArrayList;
import java.util.List;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class BlockInstruction extends LatteInstruction {
	public List<LatteInstruction> body;

	public BlockInstruction(Symbol s) {
		super(s);
		this.body = new ArrayList<LatteInstruction>();
	}

	public BlockInstruction add(LatteInstruction i) {
		body.add(i);
		return this;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
