package latte_program_tree;

import java.util.ArrayList;
import java.util.List;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class LatteProgram extends ProgramStructNode {
	public List<FunctionDeclaration> functions;
	public List<ClassDeclaration> classes;

	public LatteProgram(Symbol s) {
		super(s);
		functions = new ArrayList<FunctionDeclaration>();
		classes = new ArrayList<ClassDeclaration>();
	}

	public LatteProgram declare(FunctionDeclaration decl) {
		functions.add(decl);
		return this;
	}

	public LatteProgram declare(ClassDeclaration decl) {
		classes.add(decl);
		return this;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
