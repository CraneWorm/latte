package latte_program_tree;

import java.util.ArrayList;
import java.util.List;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class FunctionApplication extends LatteExpression {
	public String function_name;
	public LatteExpression object;
	public List<LatteExpression> args;
	public FunctionApplication(String function_name, List<LatteExpression> args, Symbol s) {
		super(s);
		this.function_name = function_name;
		this.args = new ArrayList<LatteExpression>(args);
	}
	
	public FunctionApplication method_call(LatteExpression this_object){
		this.object = this_object;
		return this;
	}
	
	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
