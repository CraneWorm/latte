package latte_program_tree;

import java_cup.runtime.Symbol;
import visitors.ProgramNodeVisitor;

public class SelfExpression extends LatteExpression {

	public SelfExpression(Symbol symbol) {
		super(symbol);
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
