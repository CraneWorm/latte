package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class NotExpression extends LatteExpression {
	public LatteExpression right;
	public NotExpression(LatteExpression e1, Symbol s) {
		super(s);
		right = e1;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
