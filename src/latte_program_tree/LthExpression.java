package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class LthExpression extends LatteExpression {

	public LatteExpression left;
	public LatteExpression right;
	public LthExpression(LatteExpression left, LatteExpression right, Symbol s) {
		super(s);
		this.left = left;
		this.right = right;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
