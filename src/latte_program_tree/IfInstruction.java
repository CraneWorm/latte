package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class IfInstruction extends LatteInstruction {
	public IfInstruction(LatteExpression condition, LatteInstruction if_branch,
			LatteInstruction else_branch, Symbol s) {
		super(s);
		this.condition = condition;
		this.if_branch = if_branch;
		this.else_branch = (else_branch != null) ? else_branch : new EmptyInstruction(null);
		
	}

	public LatteExpression condition;
	public LatteInstruction if_branch;
	public LatteInstruction else_branch;

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
