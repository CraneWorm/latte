package latte_program_tree;

import java.util.ArrayList;
import java.util.List;

public class LatteFunctionType {
	public LatteType return_type;
	public List<LatteType> argument_types;
	public String origin = null;
	public LatteFunctionType(LatteType return_type,
			List<LatteType> argument_types) {
		
		this.return_type = return_type;
		this.argument_types = new ArrayList<LatteType>(argument_types);		
	}
	public LatteFunctionType(String origin, LatteType return_type,
			List<LatteType> argument_types) {
		this(return_type, argument_types);
		this.origin = origin;
	}
	
	public boolean equals(LatteType t) {
		return false;
	}

}
