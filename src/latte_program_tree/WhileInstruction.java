package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class WhileInstruction extends LatteInstruction {
	public LatteExpression condition;
	public LatteInstruction loop;

	public WhileInstruction(LatteExpression condition, LatteInstruction loop, Symbol s) {
		super(s);
		this.condition = condition;
		this.loop = loop;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
