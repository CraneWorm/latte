package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class VarExpression extends LatteExpression {
	public String ident;
	public LatteExpression object;
	public VarExpression(String ident, Symbol s){
		super(s);
		this.ident = ident;
	}
	public VarExpression(String ident, LatteExpression this_object, Symbol s){
		super(s);
		this.ident = ident;
		this.object = this_object;
	}	
	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
