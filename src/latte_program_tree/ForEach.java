package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class ForEach extends LatteInstruction {
	public LatteVar loop_var;
	public LatteExpression array_expr;
	public LatteInstruction loop_instruction;

	public ForEach(LatteVar loop_var, LatteExpression a_expr,
			LatteInstruction loop_instruction, Symbol s) {
		super(s);
		this.loop_var = loop_var;
		this.array_expr = a_expr;
		this.loop_instruction = loop_instruction;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
