package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class DecrementInstruction extends LatteInstruction {

	public LatteExpression var;
	public DecrementInstruction(LatteExpression e, Symbol s) {
		super(s);
		this.var = e;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
