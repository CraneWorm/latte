package latte_program_tree;

import java_cup.runtime.Symbol;
import visitors.ProgramNodeVisitor;

public class ArrayAllocation extends LatteExpression {
	public LatteType type;
	public LatteExpression size_expression;
	public ArrayAllocation(LatteType latteType, LatteExpression e, Symbol symbol) {
		super(symbol);
		this.type = latteType;
		this.size_expression = e;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
