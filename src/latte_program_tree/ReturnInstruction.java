package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class ReturnInstruction extends LatteInstruction {
	
	public LatteExpression return_value;

	public ReturnInstruction(LatteExpression return_value, Symbol s) {
		super(s);	
		this.return_value = return_value;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
