package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class GeExpression extends LatteExpression {
	public LatteExpression left;
	public LatteExpression right;
	public GeExpression(LatteExpression e1, LatteExpression e2, Symbol s) {
		super(s);
		left = e1;
		right = e2;
	}
	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
