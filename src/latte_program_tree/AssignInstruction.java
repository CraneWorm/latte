package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class AssignInstruction extends LatteInstruction {

	public LatteExpression lval;
	public LatteExpression rval;

	public AssignInstruction(LatteExpression lval, LatteExpression rval, Symbol s) {
		super(s);
		this.lval = lval;
		this.rval = rval;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
