package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class DotExpression extends LatteExpression {
	public LatteExpression left;
	public LatteExpression right;
	public DotExpression(LatteExpression i1, LatteExpression i2, Symbol s) {
		super(s);
		left = i1;
		right = i2;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
