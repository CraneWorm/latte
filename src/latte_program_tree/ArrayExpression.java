package latte_program_tree;

import java_cup.runtime.Symbol;
import visitors.ProgramNodeVisitor;

public class ArrayExpression extends LatteExpression {
	public LatteExpression array;
	public LatteExpression index;
	public ArrayExpression(LatteExpression a, LatteExpression i, Symbol symbol) {
		super(symbol);
		this.array = a;
		this.index = i;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
