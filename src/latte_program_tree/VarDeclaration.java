package latte_program_tree;

import helpers.Pair;

import java.util.ArrayList;
import java.util.List;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class VarDeclaration extends LatteInstruction {
	public LatteType type;
	public List<Pair<LatteVar, LatteExpression>> vars;

	public VarDeclaration(Symbol s) {
		super(s);
		this.vars = new ArrayList<Pair<LatteVar, LatteExpression>>();
	}

	public VarDeclaration type(LatteType type) {
		this.type = type;
		for(Pair<LatteVar, LatteExpression> var: vars){
			var.left.type = type;
		}
		return this;
	}

	public VarDeclaration declare(LatteVar var, LatteExpression initializer) {
		this.vars.add(new Pair<LatteVar, LatteExpression>(var, initializer));
		return this;
	}

	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
