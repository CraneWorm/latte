package latte_program_tree;

import java_cup.runtime.Symbol;
import visitors.ProgramNodeVisitor;

public class LatteType extends ProgramStructNode {
	public String name;
	public LatteType super_type;

	public LatteType(String type, Symbol s) {
		super(s);
		this.name = type;
	}
	
	public LatteType extend(LatteType super_type){
		this.super_type = super_type;
		return this;
	}
	
	@Override
	public int hashCode(){
		return this.name.hashCode();
	}
	
	@Override
	public boolean equals(Object o){
		if (o instanceof LatteType){
			return this.name.equals(((LatteType) o).name);
		}
		return false;
	}
	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
	

}
