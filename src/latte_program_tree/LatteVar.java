package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class LatteVar extends ProgramStructNode {
	public String ident;
	public LatteType type;

	public LatteVar(LatteType type, String ident, Symbol s) {
		super(s);
		this.ident = ident;
		this.type = type;
	}
	
	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}
}
