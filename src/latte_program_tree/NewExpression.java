package latte_program_tree;

import visitors.ProgramNodeVisitor;
import java_cup.runtime.Symbol;

public class NewExpression extends LatteExpression {

	public LatteExpression expr = null;
	public NewExpression(LatteType t, Symbol s) {
		super(s);
		this.type = t;
	}

	public NewExpression(LatteType t, LatteExpression optional, Symbol s) {
		this(t, s);
		this.expr = optional;
	}
	
	@Override
	public <T> T accept(ProgramNodeVisitor<T> Visitor) {
		return Visitor.visit(this);
	}

}
