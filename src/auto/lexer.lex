package auto;
import java_cup.runtime.*;
import helpers.ErrorReporter;
%%
%public
%class Lexer
%cup
%line
%column
%eofval{ 
    return symbol( Symbols.EOF );
%eofval}

%{
  StringBuffer string = new StringBuffer();
  public ErrorReporter error_reporter;
  public Lexer(java.io.Reader in, ErrorReporter err){
  	this(in);
  	error_reporter = err;
  }
  private Symbol symbol(int type) {
    return new Symbol(type, yyline, yycolumn, yytext());
  }
  private Symbol symbol(int type, Object value) {
    return new Symbol(type, yyline, yycolumn, value);
  }
%}

LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]
InputCharacter = [^\r\n]

IntLiteral = 0 | [1-9][0-9]*
Ident = [:jletter:] [:jletterdigit:]*
BoolLiteral = true | false
ArrayType = "[" {WhiteSpace}* "]"

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} | {HashComment} | {DocumentationComment}

TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}
HashComment			 = "#"  {InputCharacter}* {LineTerminator}
DocumentationComment = "/**" {CommentContent} "*"+ "/"
CommentContent       = ( [^*] | \*+ [^/*] )*

%state STRING

%%

<YYINITIAL>
{
	\"              { string.setLength(0); yybegin(STRING); }
    {ArrayType}		{ return symbol( Symbols.TYPEARRAY ); }
    									
	"int"			{ return symbol( Symbols.TYPEINT ); }
	"string"		{ return symbol( Symbols.TYPESTRING ); }
	"boolean"		{ return symbol( Symbols.TYPEBOOLEAN ); }
    "void"			{ return symbol( Symbols.TYPEVOID ); }
	"return"		{ return symbol( Symbols.RETURN ); }
	"if"			{ return symbol( Symbols.IF ); }
	"else"			{ return symbol( Symbols.ELSE ); }
	"while"			{ return symbol( Symbols.WHILE ); }
	"new"			{ return symbol( Symbols.NEW ); }
	"class"			{ return symbol( Symbols.CLASS ); }
	"extends"			{ return symbol( Symbols.EXTENDS ); }
	"null"			{ return symbol( Symbols.NULL ); }
	"self"			{ return symbol( Symbols.SELF ); }
	"for"			{ return symbol( Symbols.FOR ); }
	"++"			{ return symbol( Symbols.INCR ); }
	"--"			{ return symbol( Symbols.DECR ); }
	"=="			{ return symbol( Symbols.EQU ); }
	"&&"			{ return symbol( Symbols.AND ); }
	"||"			{ return symbol( Symbols.OR ); }
	">"				{ return symbol( Symbols.GTH ); }
	"<"				{ return symbol( Symbols.LTH ); }
	">="			{ return symbol( Symbols.GE ); }
	"<="			{ return symbol( Symbols.LE ); }
	"!="			{ return symbol( Symbols.NE ); }
	"!"             { return symbol( Symbols.NOT ); }
	"="             { return symbol( Symbols.ASSIGN ); }
	":"             { return symbol( Symbols.COLON ); }
    "+"             { return symbol( Symbols.PLUS ); }
    "/"             { return symbol( Symbols.DIVIDE ); }    
    "*"             { return symbol( Symbols.TIMES ); }
    "-"             { return symbol( Symbols.MINUS ); }
    "%"             { return symbol( Symbols.MOD ); }
    "."             { return symbol( Symbols.DOT ); }
    "("             { return symbol( Symbols.LPAREN ); }
    ")"             { return symbol( Symbols.RPAREN ); }
    "{"             { return symbol( Symbols.LBRACE ); }
    "}"             { return symbol( Symbols.RBRACE ); }
    "["             { return symbol( Symbols.LBRACKET ); }
    "]"             { return symbol( Symbols.RBRACKET ); }
    ";"             { return symbol( Symbols.SEMI ); }
    ","             { return symbol( Symbols.COMMA ); }
    {BoolLiteral}	{ return symbol( Symbols.BOOL, new Boolean( yytext() ) ); }
    {IntLiteral}    { return symbol( Symbols.NUMBER, new Integer( yytext() ) ); }
    {Comment}       { /* ignore */ }
    {WhiteSpace}    { /* ignore */ }
    {Ident}			{ return symbol( Symbols.IDENT, yytext() ); }
}

<STRING> {
  \"                             { yybegin(YYINITIAL); 
                                   return symbol(Symbols.STRING, 
                                   string.toString()); }
  [^\n\r\"\\]+                   { string.append( yytext() ); }
  \\t                            { string.append('\t'); }
  \\n                            { string.append('\n'); }

  \\r                            { string.append('\r'); }
  \\\"                           { string.append('\"'); }
  \\                             { string.append('\\'); }
}

/* error fallback */
.|\n                             { error_reporter.report_error("Illegal character <"+
                                                    yytext()+">", "syntax", symbol(Symbols.error));
                                                    return symbol( Symbols.error ); }
                 
            
