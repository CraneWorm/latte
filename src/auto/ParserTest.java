package auto;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import latte_program_tree.FunctionDeclaration;
import latte_program_tree.LatteProgram;

import org.junit.Test;

public class ParserTest {
	private InputStream streamFromString(String chars) throws UnsupportedEncodingException{
		return new ByteArrayInputStream(chars.getBytes("UTF-8"));
	}
	
	@Test
	public void testParse() throws Exception {
		String stream = "int a(){;}";
		Parser parser = new Parser(new Lexer(streamFromString(stream)));
		LatteProgram P = (LatteProgram) parser.parse().value;
		
		assertEquals(1, P.functions.size());
		assertEquals(0, P.classes.size());
		
		FunctionDeclaration f = P.functions.get(0);
		
		assertEquals(true, f.return_type.name == "int");
		assertEquals(true, f.function_name.equals("a"));
		
		assertEquals("latte_program_tree.BlockInstruction", f.body.getClass().getName());
		assertEquals("latte_program_tree.EmptyInstruction", f.body.body.get(0).getClass().getName());
		
	}
	
	@Test
	public void testExpression() throws Exception {
		String stream = "int a(){int x = 9;}";
		Parser parser = new Parser(new Lexer(streamFromString(stream)));
		LatteProgram P = (LatteProgram) parser.parse().value;
		
		assertEquals(1, P.functions.size());
		assertEquals(0, P.classes.size());
		
		FunctionDeclaration f = P.functions.get(0);
		
		assertEquals(true, f.return_type.name == "int");
		assertEquals(true, f.function_name.equals("a"));
		
		assertEquals("latte_program_tree.BlockInstruction", f.body.getClass().getName());
		
	}
	

}
