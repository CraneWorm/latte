package x86;

import static latteQuadCode.OP.add;
import static latteQuadCode.OP.call;
import static latteQuadCode.OP.cmp;
import static latteQuadCode.OP.div;
import static latteQuadCode.OP.imul;
import static latteQuadCode.OP.inc;
import static latteQuadCode.OP.mov;
import static latteQuadCode.OP.ret;
import static latteQuadCode.OP.sub;

import java.util.HashMap;
import java.util.HashSet;

import latteQuadCode.CodeBlock;
import latteQuadCode.OP;
import latteQuadCode.QAddr;
import latteQuadCode.QLabel;
import latteQuadCode.QNode;
import latteQuadCode.QNum;
import latteQuadCode.QString;
import latteQuadCode.Quadruple;
import latteQuadCode.QuadrupleClass;
import latteQuadCode.QuadrupleCode;
import latteQuadCode.QuadrupleProgram;

public class AsmGenerator {
	private static final String movl = "movl";
	private static final String push = "push";
	private static final String pop = "pop";
	private static final String pushl = "pushl";
	private static final String popl = "popl";
	private static final String imull = "imull";
	private static final String idivl = "idivl";
	private static final String cdq = "cdq";
	private static final String incl = "incl";
	private static final String decl = "decl";
	private static final String addl = "addl";
	private static final String leal = "leal";
	private static final QReg eax = new QReg("%eax");
	private static final QReg ecx = new QReg("%ecx");
	private static final QReg edx = new QReg("%edx");
	private static final QReg ebx = new QReg("%ebx", true);
	private static final QReg edi = new QReg("%edi", true);
	private static final QReg esi = new QReg("%esi", true);
	private QuadrupleCode quadrupleCode;
	private QuadrupleProgram program;
	private StringBuilder sb;
	private HashMap<String, QuadrupleClass> classes; 
	private HashMap<QAddr, QReg> store;
	
	private HashSet<QReg> regs_busy;
	private HashSet<QReg> regs_free;
	public String code(QuadrupleProgram qp){
		program = qp;
		classes = new HashMap<String, QuadrupleClass>();
		sb = new StringBuilder();
		_(".data");
		_("# strings used in program");
		for (QString str: qp.strings){
			_(str.id()+": .string", str.toString()+";");
		}
		_("_cat_fmt_: .string \"%s%s\";");
		_("# vtables");
		for (QuadrupleClass cls: qp.classes){
			classes.put(cls.class_name(), cls);
			_(cls.class_name() + "__vtable:");
			for (String method_name: cls.vtable()){
				_("\t.long", method_name);
			}
		}
		_(".text");
		for (QuadrupleCode code: qp.functions){
			code(code);
		}
		return sb.toString();
	}
	
	@SuppressWarnings("serial")
	private void code(QuadrupleCode qc){
		store = new HashMap<QAddr, QReg>();
		regs_busy = new HashSet<QReg>();
		regs_free = new HashSet<QReg>(){
			{
				add(eax);
				add(ecx);
				add(edx);
				add(ebx);
				add(edi);
				add(esi);
			}
		};
		quadrupleCode = qc;
		
		int offset = 0;
		int arg_offset = 1;
		for (QAddr var: qc.locals().values()){
			var.loc(--offset*4 + "(%ebp)");
		}
		for (QAddr arg: qc.args()){
			arg.loc(++arg_offset*4 + "(%ebp)");
			qc.locals().put(arg.id(), arg);
		}
		_(".globl", qc.name());
		_(qc.name() + ":");
		_(push, "%ebp");
		_(mov, "%esp", "%ebp" );
		_(sub, "$"+ qc.locals().size()*4, "%esp");
		_(pushl, ebx);
		_(pushl, edi);
		_(pushl, esi);
		for (CodeBlock b: quadrupleCode.blocks()){
			if (b.jumps().size() > 0){
				_("." + b.label() + ":");
			}
			for (Quadruple q: b.quads()){
				OP op = q.op();
				switch(op){
					case add: case sub: case imul:
						gen_binary(op, q.args()[0], q.args()[1], q.args()[2]);
						break;
					case cmp:
						gen_cmp(q);
						break;
					case cat:
						gen_cat(q);
						break;
					case div: case mod:
						gen_div(q, op);
						break;
					case mov:
						gen_mov(q);
						break;
					case arg:
						gen_arg(q);
						break;
					case call:
						gen_call(q);
						break;
					case inc: case dec:
						gen_inc(q, op);						
						break;
					case je: case jge: case jg: case jle:	case jl: case jne: case jmp:
						gen_jump(q, op);
						break;
					case ret:
						gen_ret(q);
						break;
					case a_alloc:
						gen_a_alloc(q);
						break;
					case put_i:
						gen_put_i(q);
						break;
					case get_i:
						gen_get_i(q);
						break;
					case len:
						gen_len(q);
						break;
					case call_method:
						gen_call_method(q);
						break;
					case get_m:
						gen_get_m(q);						
						break;
					case set_m:
						gen_set_m(q);						
						break;
					case _new:
						gen_new(q);
						break;
					default:
						break;
				}
			}
			forget_regs();
			store.clear();
		}
		gen_ret(new Quadruple(ret, new QNode[0]));
	}

	private void gen_new(Quadruple q) {
		String class_name = q.args()[1].id();
		int size = 4*(classes.get(class_name).members_size()+1);
		_(pushl,"$"+size);
		freeReg(eax);
		_(call, "malloc");
		_(movl, "$"+class_name+"__vtable", "(%eax)");
		_(movl, eax, ((QAddr)q.args()[0]).loc());
		_(add, "$4", "%esp");
	}

	private void gen_set_m(Quadruple q) {
		QAddr self = (QAddr) q.args()[0];
		String attr_str = q.args()[1].id();
		QNode val = q.args()[2];
		String class_name = attr_str.substring(0, attr_str.indexOf("::"));
		int attr_index = classes.get(class_name).member_offset(attr_str) +1;
		
		QReg 	regb = regs_free.isEmpty() ? 
					freeReg(regs_busy.iterator().next()) : 
						regs_free.iterator().next();
		regs_free.remove(regb);
		QReg	regv = regs_free.isEmpty() ? 
				freeReg(regs_busy.iterator().next()) : 
					regs_free.iterator().next();
		_(movl, loc(self), regb);		
		_(movl, loc(val), regv);
		_(movl, regv, 4*attr_index+"(" + regb + ")");
		forget(regb);
	}

	private void gen_get_m(Quadruple q) {
		QAddr ret 	= (QAddr) q.args()[0];
		QAddr self = (QAddr) q.args()[1];
		String attr_str = q.args()[2].id();
		String class_name = attr_str.substring(0, attr_str.indexOf("::"));
		int attr_index = classes.get(class_name).member_offset(attr_str) +1;
		
		QReg 	regb = regs_free.isEmpty() ? 
					freeReg(regs_busy.iterator().next()) : 
						regs_free.iterator().next();								
		_(movl, loc(self), regb);
		_(movl, 4*attr_index+"(" + regb + ")",regb);
		_(movl, regb, ret.loc());
	}

	private void gen_call_method(Quadruple q) {
		save_regs(false);
		String[] s = q.args()[1].id().split("__");
		int method_offset = 4*classes.get(s[0]).method_offset(s[1]);
		_(mov, "(%esp)", eax);
		_(mov, "(%eax)", eax);
		_(call, "*"+ method_offset + "(%eax)");
		eax.val = (QAddr) q.args()[0];
		store.put(eax.val, eax);
		regs_free.remove(eax);
		regs_busy.add(eax);
		_(add, "$" + 4*((QNum)q.args()[2]).val(), "%esp");
	}

	private void gen_len(Quadruple q) {
		QAddr ret 	= (QAddr) q.args()[0];
		QAddr array = (QAddr) q.args()[1];
		QReg 	regb = regs_free.isEmpty() ? 
					freeReg(regs_busy.iterator().next()) : 
						regs_free.iterator().next();
		_(movl, loc(array), regb);
		_(leal, "-4("+regb+")", regb);
		_(movl, "("+ regb +")", regb);
		_(movl, regb, ret.loc());
	}
	
	private void gen_get_i(Quadruple q) {
		QAddr ret 	= (QAddr) q.args()[0];
		QAddr array = (QAddr) q.args()[1];
		QNode index = q.args()[2];
		QReg 	regb = regs_free.isEmpty() ? 
					freeReg(regs_busy.iterator().next()) : 
						regs_free.iterator().next();
				regs_free.remove(regb);
		QReg	regi = regs_free.isEmpty() ? 
						freeReg(regs_busy.iterator().next()) : 
							regs_free.iterator().next();
		_(movl, loc(array), regb);
		_(movl, loc(index), regi);
		_(leal, "(" + regb + "," + regi + ", 4)", regb);
		_(movl, "("+ regb +")", regi);
		_(movl, regi, ret.loc());
		forget(regb);
	}

	private void gen_put_i(Quadruple q) {
		QAddr arr = (QAddr) q.args()[0];
		QNode index = q.args()[1];
		QNode value = q.args()[2];
		freeReg(eax);
		regs_free.remove(eax);
		regs_busy.add(eax);
		QReg reg = regs_free.isEmpty() ?
				freeReg(regs_busy.iterator().next()) :
					regs_free.iterator().next();
		_(movl, loc(arr), eax);
		_(movl, loc(index), reg);
		_(leal , "(" + eax + ","+ reg +", 4)", eax);
		_(movl, loc(value), reg);
		_(movl, reg, "(%eax)");
	}

	private void gen_a_alloc(Quadruple q) {
		QAddr arr = (QAddr) q.args()[0];
		QNode size = q.args()[1];
		freeReg(eax);
		regs_free.remove(eax);
		QReg reg = regs_free.isEmpty() ?
				freeReg(regs_busy.iterator().next()) :
					regs_free.iterator().next();
		regs_busy.add(eax);
		_(movl, "$4", eax);
		_(imul, loc(size), eax);
		_(add, "$4", eax);
		_(pushl, eax);
		_(call, "malloc");
		_(movl, eax, arr.loc());
		_(movl, loc(size), reg);
		_(movl, reg, "(%eax)");
		_(addl, "$4", arr.loc());
		forget(eax);
		_(add, "$4", "%esp");		
	}

	private void gen_cat(Quadruple q) {
		QNode sl = q.args()[1], sr = q.args()[2];
		QAddr str = (QAddr) q.args()[0];
		freeReg(eax);
		freeReg(ebx);
		_(pushl, loc(sr));      // +1
		_(call, "strlen");
		_(movl, eax, ebx);
		_(pushl, loc(sl));      // +2
		_(call, "strlen");
		_(add, eax, ebx);
		_(incl, ebx);
		_(pushl, ebx);          // +3
		_(call, "malloc");
		_(movl, "$_cat_fmt_", "(%esp)");
		_(movl, eax, loc(str));						
		_(pushl, ebx);          // +4
		_(pushl, eax);          // +5
		_(call, "snprintf");
		_(add, "$20", "%esp");	// -5
	}

	private void forget_regs() {
		@SuppressWarnings("unchecked")
		HashSet<QReg> busy_copy = (HashSet<QReg>) regs_busy.clone();
		for (QReg reg: busy_copy){
			forget(reg);
		}
	}
	
	private void save_regs(boolean callee_saved) {
		@SuppressWarnings("unchecked")
		HashSet<QReg> busy_copy = (HashSet<QReg>) regs_busy.clone();
		for (QReg reg: busy_copy){
			if (!reg.callee_saved || callee_saved){
				freeReg(reg);
			}
		}
	}
	
	private void gen_inc(Quadruple q, OP op) {
		String idc = op.equals(inc) ? incl : decl; 
		QNode var = q.args()[0];
		_(idc, loc(var));
		QReg reg = store.get(var);
		if (reg != null){
			_(idc, reg);
		}
	}

	private void gen_div(Quadruple q, OP op) {
		QNode m = q.args()[1], n = q.args()[2];
		QReg reg = store.get(m);
		freeReg(eax);
		_(movl, loc(m), eax);
		if (regs_busy.contains(edx)){
			freeReg(edx);
		}
		_(cdq);
		if (is_local(n)){
			_(idivl, loc(n));
		} else {
			freeReg(ecx);
			loadVar(n, ecx);
			_(idivl, ecx);
			forget(ecx);
		}
		if (op.equals(div)){
			forget(edx);
			store.remove(eax.val);
			eax.val = (QAddr) q.args()[0]; 
			store.put(eax.val, eax);
			regs_free.remove(eax);
			regs_busy.add(eax);
		} else {
			forget(eax);
			store.remove(edx.val);
			edx.val = (QAddr) q.args()[0]; 
			store.put(edx.val, edx);
			regs_free.remove(edx);
			regs_busy.add(edx);
		}
	}

	private void gen_jump(Quadruple q, OP op) {
		_(op, "." + ((QLabel)q.args()[0]).id());
	}

	private void gen_ret(Quadruple q) {
		if (q.args().length > 0){
			QNode r = q.args()[0];
			QReg reg;
			if (is_local(r) && (reg = store.get(r)) != null){
				_(mov, reg, eax);
			} else {
				loadVar(r, eax);
			}
		}
		_(popl, esi);
		_(popl, edi);
		_(popl, ebx);
		_(mov, "%ebp", "%esp");
		_(pop, "%ebp");
		_(ret);
	}

	private void gen_arg(Quadruple q) {
		QReg reg; QNode a = q.args()[0]; 
		if (is_local(a) &&
				(reg = store.get(a)) != null){			
			_(push, reg);							
		} else {
			_(pushl, loc(a));
		}
	}

	private void gen_call(Quadruple q) {
		save_regs(false);
		_(call, q.args()[1].id());
		eax.val = (QAddr) q.args()[0];
		store.put(eax.val, eax);
		regs_free.remove(eax);
		regs_busy.add(eax);
		_(add, "$" + 4*((QNum)q.args()[2]).val(), "%esp");
	}

	private void gen_cmp(Quadruple q) {
		QReg reg = store.get(q.args()[0]);
		if (reg != null){
			_(cmp, loc(q.args()[1]), reg);
		} else {
			if (regs_free.isEmpty()){
				reg = freeReg(regs_busy.iterator().next());
			} else {
				reg = regs_free.iterator().next();
			}
			loadVar(q.args()[0], reg);
			_(cmp, loc(q.args()[1]), reg);
		}
	}

	private void gen_mov(Quadruple q) {
		QReg reg = store.get(q.args()[1]);
		QAddr lval = (QAddr)q.args()[0];
		if (reg != null){
			QAddr rval = (QAddr)q.args()[1];
			_(movl, reg, lval.loc());
			store.remove(rval);
			store.put(lval, reg);
			reg.val = lval;
		} else {
			QNode rval = q.args()[1];
			if (regs_free.isEmpty()){
				reg = freeReg(regs_busy.iterator().next());
			} else {
				reg = regs_free.iterator().next();
			}
			loadVar(rval, reg);
			_(movl, reg, lval.loc());
		}
	}
	
	private String loc(QNode node){
		String ret;
		if (node instanceof QAddr){
			QAddr addr = (QAddr)node;
			QReg reg = store.get(node);
			if (reg != null){
				ret = reg.name;
			} else {
				ret = ((QAddr) node).loc();
			}
		} else {
			ret = "$"  + node.id();
		}
		return ret;
	}
	
	private QReg freeReg(QReg reg){
		if (reg.val != null){
			_(movl, reg, reg.val.loc());
		}
		regs_busy.remove(reg);
		regs_free.add(reg);
		store.remove(reg.val);
		reg.val = null;
		return reg;
	}
	
	private void forget(QReg reg){
		regs_busy.remove(reg);
		regs_free.add(reg);
		store.remove(reg.val);
		reg.val = null;
	}
	
	private void loadVar(QNode var, QReg reg){
		_(movl, loc(var), reg);
		if (is_local(var)){
			reg.val = (QAddr)var;
			store.put((QAddr)var, reg);
		} else {
			reg.val = null;
		}
		regs_free.remove(reg);
		regs_busy.add(reg);
	}
	
	
	private void gen_binary(OP op, QNode result, QNode left, QNode right){
		QReg regl, regr;
		if (store.containsKey(left)){
			regl = store.get(left);
		} else {
			if (regs_free.isEmpty()){
				regl = freeReg(regs_busy.iterator().next());
			} else {
				regl = regs_free.iterator().next();
			}
			loadVar(left, regl);
		}
		if (store.containsKey(right)){
			regr = store.get(right);
			_(op, regr, regl);
		} else {
			_(op, loc(right), regl);
		}
		store.remove(regl.val);
		regl.val = (QAddr)result;
		store.put((QAddr)result, regl);
		regs_free.remove(regl);
		regs_busy.add(regl);
	}
	
	private boolean is_num(QNode var){
		return var instanceof QNum;
	}
	
	private boolean is_string(QNode var){
		return var instanceof QString;
	}
	
	private boolean is_local(QNode var){
		return var instanceof QAddr;
	}	

	private void _( Object... line){
		if (!line[0].toString().startsWith(".")) sb.append("\t");
		switch(line.length){
			case 1:
				sb.append(line[0]+"\n");
				break;
			case 2:
				sb.append(line[0] + " " + line[1]+"\n");
				break;
			case 3:
				sb.append(line[0] + " " + line[1] + ", " + line[2]+"\n");
				break;
			default:
				break;
		}
	}
}
