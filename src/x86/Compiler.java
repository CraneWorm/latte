package x86;

import helpers.ErrorReporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import latte_program_tree.LatteProgram;
import visitors.ExpressionRewriter;
import visitors.JasminCodeGenerator;
import visitors.QuadrupleCodeGenerator;
import visitors.ReturnChecker;
import visitors.TypeChecker;
import visitors.VisitorPipe;
import auto.Lexer;
import auto.Parser;

public class Compiler {
	public static void main(String[] args){
		if (args.length == 0) System.exit(0);
		File source = new File(args[0]);
		ErrorReporter err = new ErrorReporter();
		try {
			Parser parser = new Parser(new Lexer(new FileReader(source), err), err);
			LatteProgram P = (LatteProgram) parser.parse().value;
			String asm_name = source.getName().split(".lat")[0];
			QuadrupleCodeGenerator qcg = new QuadrupleCodeGenerator();
			if (P != null){
				err = VisitorPipe._(P, 
					new TypeChecker(),
					new ExpressionRewriter(),
					new ReturnChecker(),
					qcg
				);				
			}
			if (err.error_count() == 0){
				System.err.print("OK\n");
				System.err.print(err.errorSummary());
				String sPath = source.getParent()+ "/" + asm_name + ".s";
				String qPath = source.getParent()+ "/" + asm_name + ".q";
				File q_file = new File(qPath);
				FileWriter qwriter = new FileWriter(q_file);
				qwriter.write(qcg.toString());
				qwriter.flush();
				qwriter.close();
				File asm_file = new File(sPath);
				FileWriter writer = new FileWriter(asm_file);
				writer.write((new AsmGenerator()).code(qcg.program()));
				writer.flush();
				writer.close();
				// apparently java can't be bothered enough to run gcc
				// so it's called from the main script
				System.exit(0);
			} else {
				System.err.print("ERROR\n");
				System.err.print(err.errorSummary());
				System.exit(1);
			}			 
		} catch (FileNotFoundException e) {
			System.err.print("file not found: "+args[0]);
			System.exit(1);
		} catch (Exception e) {
			System.err.print("ERROR\n");
			System.err.print(err.errorSummary());
			System.exit(1);
		}
	}
}
