package x86;

import latteQuadCode.QAddr;

public class QReg {
	String name;
	public QAddr val;
	boolean callee_saved = false;
	public QReg(String name){ this.name = name; }
	public QReg(String name, boolean callee_saved){ 
		this.name = name;
		this.callee_saved = callee_saved;
		this.val = null;
	}
	public String toString() { return name;}
}
