import java.util.Scanner;

public class Runtime {
	static {
		in = new Scanner(System.in);		
	}
	private static Scanner in;
	public static void printInt(int i){ System.out.println(i); }
	public static void printString(String i){ System.out.println(i); }
	public static int readInt(){
		in.useDelimiter("[\n\r\t ]+");
		return new Integer(in.next());		
	}
	public static String readString(){
		in.useDelimiter("\n");
		return in.next();
	}
	public static void error(){
		System.out.println("runtime error");
		System.out.flush();
		in.close();
		System.exit(1);
	}
}
