package latte;

import helpers.ErrorReporter;
import helpers.FileFinder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import latte_program_tree.LatteProgram;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import visitors.ExpressionRewriter;
import visitors.ProgramTreePrinter;
import visitors.ReturnChecker;
import visitors.TypeChecker;
import visitors.VisitorPipe;
import auto.Lexer;
import auto.Parser;

@RunWith(value = Parameterized.class)
public class CompilerTest {

	private void print(Object o) {
		System.out.println(o);
		System.out.flush();
	}

	private String basePath;
	
	public CompilerTest(String basePath){
		this.basePath = basePath;
	}
	
	@SuppressWarnings("rawtypes")
	@Parameters(name= "{index}:{0}")
	public static Collection data() {
		FileFinder sourceFinder = new FileFinder("*.lat");
        Path startingDir = Paths.get("lattests");
		try {
			Files.walkFileTree(startingDir, sourceFinder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String[]> sourceArgs = new LinkedList<String[]>();
		List<String> results = sourceFinder.getResults();
		Collections.sort(results);
		for (String path: results){
			sourceArgs.add(new String[]{path});
			System.out.println(path);
		}
		return sourceArgs;
	}

	@Test
	public void SourceTest() throws FileNotFoundException, Exception {
		File f = new File(basePath);
		print(f.getName());
		ErrorReporter err = new ErrorReporter();
		Parser parser = new Parser(new Lexer(new FileReader(f), err), err);
		LatteProgram P = (LatteProgram) parser.parse().value;
		if (P != null) {
			err = VisitorPipe._(P,
				new TypeChecker(),
				new ExpressionRewriter(),
				new ReturnChecker(),
				new ProgramTreePrinter()
			);
		}
		print(err.errorSummary());
	}

}
