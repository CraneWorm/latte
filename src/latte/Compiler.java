package latte;

import helpers.ErrorReporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import latte_program_tree.LatteProgram;
import visitors.ExpressionRewriter;
import visitors.JasminCodeGenerator;
import visitors.ReturnChecker;
import visitors.TypeChecker;
import visitors.VisitorPipe;
import auto.Lexer;
import auto.Parser;

public class Compiler {
	public static void main(String[] args){
		if (args.length == 0) System.exit(0);
		File source = new File(args[0]);
		ErrorReporter err = new ErrorReporter();
		try {
			Parser parser = new Parser(new Lexer(new FileReader(source), err), err);
			LatteProgram P = (LatteProgram) parser.parse().value;
			String class_name = source.getName().split(".lat")[0];
			String source_name = class_name + ".j";
			JasminCodeGenerator jcg = new JasminCodeGenerator(source_name, class_name);
			if (P != null){
				err = VisitorPipe._(P, 
					new TypeChecker(),
					new ExpressionRewriter(),
					new ReturnChecker(),
					jcg
				);				
			}
			if (err.error_count() == 0){
				System.err.print("OK\n");
				System.err.print(err.errorSummary());
				String jasminPath = source.getParent()+ "/" + source_name;
				String outPath = source.getParent();
				File j = new File(jasminPath);
				FileWriter writer = new FileWriter(j);
				writer.write(jcg.code());
				writer.flush();
				writer.close();
				java.lang.Runtime.getRuntime().exec(
						String.format(
								"java -jar lib/jasmin.jar -d %s %s",
								outPath,
								jasminPath
						)
				).waitFor(); //essential for bigger files
				System.exit(0);
			} else {
				System.err.print("ERROR\n");
				System.err.print(err.errorSummary());
				System.exit(1);
			}			 
		} catch (FileNotFoundException e) {
			System.err.print("file not found: "+args[0]);
			System.exit(1);
		} catch (Exception e) {
			System.err.print("ERROR\n");
			System.err.print(err.errorSummary());
			System.exit(1);
		}
	}
}
