package latteQuadCode;


public class QAddr extends QNode {
	
	private static int c = 0;
	private static final String tmp_tmpl = "_t%d_";
	
	private String id;
	private String name;
	private String loc = null;
	
	public String id(){ return id; }
	public String name(){ return name; }
	public String toString(){ return name+"@"+id; }
	public QAddr(String var){
		name = var;
		id = String.format(tmp_tmpl, ++c);
	}
	public QAddr(){
		name = "";
		id = String.format(tmp_tmpl, ++c);
	}
	public void loc(String l){ loc = l; }
	public String loc() { return loc; }
	public static void resetc(){ c = 0; }
	public static void resetc(int r){ c = r; }
	public static int c(){ return c; }
	public int hashCode(){
		return id.hashCode();
	}
	public boolean equals(Object o){
		return o instanceof QAddr && ((QAddr)o).id.equals(id);
	}
}
