package latteQuadCode;

public class QNum extends QNode {
	private int val;
	
	public int val(){return this.val; };
	public void val(int val){this.val = val; }
	public QNum(){ this.val = 0; }
	public QNum(int val){this.val = val; }	
	public String toString(){ return ""+val; }
	public String id(){return this.val + ""; }
}
