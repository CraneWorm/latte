package latteQuadCode;

import java.util.HashMap;

public class QString extends QNode {
	private static HashMap<String, QString> strings;
	static {
		strings = new HashMap<String, QString>();
	}
	private static int scount = 0;
	private String id;
	private String val;
	public String id(){return this.id; }
	public String val(){return this.val; }
	public void val(String val){ this.val = val; }
	private QString(String val){
		this.val = val;
		this.id = "_s" + (++scount) + "_";
		strings.put(val, this);
	}
	public static QString string(String val){
		return strings.containsKey(val) ? strings.get(val) : new QString(val);
	}
	public boolean equals(Object o){
		return (o instanceof QString) && ((QString)o).val.equals(this.val);
	}
	public String toString(){ return '"' 
			+ val
			.replaceAll("([\n])", "\\\\n")
			.replaceAll("\\r","\\\\r")
			.replaceAll("\\t","\\\\t")
			.replaceAll("\\\"", "\\\\\"") + '"';
	}
}
