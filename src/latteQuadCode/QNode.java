package latteQuadCode;

public abstract class QNode {
	protected Quadruple parent;
	protected Quadruple parent(){ return parent; }
	protected void parent( Quadruple p){
		parent = p;
	}
	public abstract String id();
}
