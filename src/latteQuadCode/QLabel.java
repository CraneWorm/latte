package latteQuadCode;

public class QLabel extends QNode {
	private static int c = 0;
	private static final String tmpl = "L%d";
	private String id;
	private QLabel(String id){
		this.id = id;
	}
	public String id(){return this.id; }
	public String toString(){ return id; }
	public static QLabel fresh(){
		return new QLabel(String.format(tmpl, ++c));
	}
}
