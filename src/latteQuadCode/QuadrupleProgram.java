package latteQuadCode;

import java.util.HashMap;
import java.util.LinkedList;

public class QuadrupleProgram {
	public LinkedList<QuadrupleClass> classes;
	public LinkedList<QuadrupleCode> functions;
	public LinkedList<QString> strings;
	public QuadrupleProgram(LinkedList<QuadrupleClass> classes, 
			LinkedList<QuadrupleCode> functions, 
			LinkedList<QString> strings){
		this.classes = classes;
		this.functions = functions;
		this.strings = strings;
	}
}
