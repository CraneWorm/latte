package latteQuadCode;


public class Quadruple extends QNode {
	
	private OP op;
	private QNode[] args;
	private Quadruple next;
	private Quadruple prev;
	public String id(){return this.op.name(); }
	public OP op(){ return op; }
	public QNode[] args(){ return args; }
	public Quadruple next(){ return next; }
	public Quadruple prev(){ return prev; }
	
	public Quadruple next(Quadruple q){ 
		next = q;
		if (q != null)
			q.prev = this;
		return q;
	}

	public Quadruple prev(Quadruple q){ 
		prev = q;
		if (q != null)
			q.next = this;
		return q;
	}
	
	public Quadruple(OP op, QNode... args) {
		this.op = op;
		this.args = args;
		for (QNode n: args){
			n.parent(this);
		}
		next = null;
	}
	
	public String toString(){
		String ret = "(" + op.name();
		for (QNode v: args){
			ret += ", " + v; 
		}
		ret += ")";
		return ret;
	}
}
