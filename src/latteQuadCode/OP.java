package latteQuadCode;

public enum OP {
	// general purpose copy operator
	mov,
	// values
	val,
	// integer operators
	add, sub, imul, div, mod, inc, dec, cmp,
	// string operator
	cat,
	// jumps
	jmp, jl, jg, jle, jge, je, jne, label,
	// function calls
	call, arg, ret,
	// array expressions
	put_i, get_i, a_alloc, len,
	// objects
	call_method, get_m, set_m, _new, self,
	// no operation
	nop;
	
	static boolean is_jump(OP op){
		switch(op){
		case jmp: case je: case jge: case jg: case jle: case jl: case jne: case ret:
			return true;
		default: 
			return false;
		}
	}	
}
