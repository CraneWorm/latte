package latteQuadCode;

public class QName extends QNode {
	public String name;
	public QName(String name){ this.name = name; }
	public String id(){return this.name; }
	public String toString(){ return this.name; }
}

