package latteQuadCode;

import java.util.LinkedList;

public class CodeBlock {
	private LinkedList<Quadruple> quads;
	// jump destination, might be null
	private CodeBlock jump;
	// next block in code, might be null
	private CodeBlock next;
	// list of blocks that pass control to this block
	private LinkedList<CodeBlock> jumps;
	// label of this block
	private QLabel label;
	
	public LinkedList<Quadruple> quads(){
		return quads;
	}
	public void jumpTo(CodeBlock qb){
		jump = qb;
	}
	public void jumpFrom(CodeBlock qb){
		jumps.add(qb);
	}
	public void nextBlock(CodeBlock qb){
		next = qb;
	}
	
	public void label(QLabel lab){
		label = lab;
	}
	
	public CodeBlock jump(){ return jump; }
	public CodeBlock next(){ return next; }
	public LinkedList<CodeBlock> jumps(){ return jumps;	}
	public QLabel label(){ return label; }
	
	public CodeBlock(LinkedList<Quadruple> qds){
		quads = qds;
		jump = null;
		next = null;
		label = null;
		jumps = new LinkedList<CodeBlock>();
	}
}
