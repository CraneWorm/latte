package latteQuadCode;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static latteQuadCode.OP.*; 

public class QuadrupleCode {
	// entry block for the procedure
	private CodeBlock entry;
	// list of simple blocks
	private LinkedList<CodeBlock> blocks;
	// maps labels to blocks
	private HashMap<QLabel, CodeBlock> labels;
	private HashMap<String, QAddr> locals;
	private LinkedList<QAddr> args;
	
	private String name;
	private void generateBlocks(List<Quadruple> quadruples){
		OP op = null;
		Quadruple qd = null;
		CodeBlock block = null;
		Iterator<Quadruple> qds = quadruples.iterator();
		while (qds.hasNext()){
			LinkedList<Quadruple> quads = new LinkedList<Quadruple>();
			block = new CodeBlock(quads);
			do {
				qd = qds.next();
				op = qd.op();
				if (op == label){
					blocks.add(block);
					quads = new LinkedList<Quadruple>();
					block = new CodeBlock(quads);
					QLabel lab = (QLabel)qd.args()[0];
					labels.put(lab, block);
					block.label(lab);
				} else {
					quads.add(qd);
				}
			} while (qds.hasNext() && !is_jump(op));
			if (blocks.size() > 0 && 
					blocks.getLast().quads().size() > 0 &&
					blocks.getLast().quads().getLast().op() != jmp &&
					blocks.getLast().quads().getLast().op() != ret){
				blocks.getLast().nextBlock(block);
			}
			blocks.add(block);
		}
	}
	
	private void setJumps(){
		// iterate over the blocks to set the jumps
		OP op = null;
		Quadruple qd = null;
		CodeBlock block = null;
		Iterator<CodeBlock> qbs = blocks.iterator();
		while (qbs.hasNext()){
			block = qbs.next();
			if (block.quads().size() > 0){
				qd = block.quads().getLast();
				op = qd.op();
				if (is_jump(op) && op != ret){
					CodeBlock cb = labels.get((QLabel)qd.args()[0]);
					block.jumpTo(cb);
					cb.jumpFrom(block);					
				}
			}
		}
	}
	public Iterator<Quadruple> ListIterator(){
		return new Iterator<Quadruple>(){
			private Iterator<CodeBlock> blocksIterator = blocks.iterator(); 
			private Iterator<Quadruple> currentBlockIterator = 
					blocksIterator.hasNext() ? blocksIterator.next().quads().iterator() : null;
			@Override
			public boolean hasNext() {
				return (currentBlockIterator != null) && (currentBlockIterator.hasNext() || (blocksIterator.hasNext() && 
						(currentBlockIterator = blocksIterator.next().quads().iterator()).hasNext()));
			}

			@Override
			public Quadruple next() {
				return (currentBlockIterator != null) && currentBlockIterator.hasNext() ? 
						currentBlockIterator.next() : 
						(currentBlockIterator = blocksIterator.next().quads().iterator()).next();
			}

			@Override
			public void remove(){}
		};
	}
	
	public void locals(HashMap<String, QAddr> l){ locals = l; }
	public void args(LinkedList<QAddr> a){ args = a; }
	public HashMap<String, QAddr> locals(){ return locals; }
	public LinkedList<QAddr> args(){ return args; }
	public String name(){ return name; }
	public void name(String n){ name = n; }
	public LinkedList<CodeBlock> blocks() { return blocks; }
	public QuadrupleCode(List<Quadruple> quadruples){
		blocks = new LinkedList<CodeBlock>();
		labels = new HashMap<QLabel, CodeBlock>();
		generateBlocks(quadruples);
		setJumps();
	}
}
