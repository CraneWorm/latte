package latteQuadCode;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import latte_program_tree.ClassDeclaration;

public class QuadrupleClass {
	private String class_name;
	private ArrayList<String> vtable;
	private LinkedHashMap<String, Integer> methods;
	private LinkedHashMap<String, Integer> members;
	
	public QuadrupleClass extend(ClassDeclaration class_declaration){
		QuadrupleClass extension = new QuadrupleClass(methods, members);
		extension.vtable = new ArrayList<String>(vtable);
		extension.class_name = class_declaration.class_name;
		for (String method: class_declaration.methods.keySet()){
			if (extension.methods.containsKey(method)){
				extension.vtable.set(extension.methods.get(method),
						extension.class_name + "__" +method);
			} else {
				extension.vtable.add(extension.class_name + "__" +method);
				extension.methods.put(method, extension.methods.size());
			}			
		}
		for (String attr: class_declaration.attributes.keySet()){
			extension.members.put(class_declaration.class_name + "::" + attr, extension.members.size());
		}
		
		return extension;
	}

	public QuadrupleClass(){
		vtable = new ArrayList<String>();
		methods = new LinkedHashMap<String, Integer>();
		members = new LinkedHashMap<String, Integer>();
	}
	
	public QuadrupleClass(LinkedHashMap<String, Integer> methods, LinkedHashMap<String, Integer> members){
		this.methods = new LinkedHashMap<String, Integer>(methods);
		this.members = new LinkedHashMap<String, Integer>(members);
	}
	
	public int method_offset(String method_name){
		return methods.get(method_name);
	}
	
	public int member_offset(String member_name){
		return members.get(member_name);
	}
	
	public ArrayList<String> vtable(){
		return vtable;
	}
	public String class_name(){
		return class_name;
	}
	public int members_size(){
		return members.size();
	}
}
