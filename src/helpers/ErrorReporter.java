package helpers;

import java.util.ArrayList;
import java.util.HashMap;

import java_cup.runtime.Symbol;

public class ErrorReporter {
	protected int error_counter;
	protected int warning_counter;
	protected HashMap<String, ArrayList<String>> errors;
	protected HashMap<String, ArrayList<String>> warnings;
	
	private void append_error(HashMap<String, ArrayList<String>> map,
			String message, String context){
		if (!map.containsKey(context))
			map.put(context, new ArrayList<String>());
		map.get(context).add(message);
	}
	public ErrorReporter(){
		errors = new HashMap<String, ArrayList<String>>();
		warnings = new HashMap<String, ArrayList<String>>();
	}
	public String errorSummary(){
		String summary = "";
		for (String context: errors.keySet()){
			for (String error: errors.get(context)){
				summary += error + "\n";
			}
		}
		for (String context: warnings.keySet()){
			for (String warning: warnings.get(context)){
				summary += warning + "\n";
			}
		}
		return summary;
	}
	public int error_count(){ return error_counter; }
	public void report_error(String message, String context, Symbol sym){
		error_counter++;
		append_error(
			errors,
			String.format(
				"%d:error: %s",
				sym.left + 1,
				message
			),
			context
		);
	}
	
	public void report_warning(String message, String context, Symbol sym){
		warning_counter++;
		append_error(
			warnings,
			String.format(
				"%d:warning: %s",
				sym.left + 1,
				message
			),
			context
		);
	}	
}
