package helpers;
//that java doesn't have generic Pair type amazes me
//below is a quick and dirty implementation of what I need

public class Pair<L, R> {
	public L left;
	public R right;
	public Pair(L left, R right){
		this.left = left;
		this.right = right;
	}
}
