package helpers;
/**
 * this is heavily based on file finding example by oracle
 * http://docs.oracle.com/javase/tutorial/essential/io/find.html
 */
import static java.nio.file.FileVisitResult.CONTINUE;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class FileFinder
		extends SimpleFileVisitor<Path> {

	private final PathMatcher matcher;
	private List<String> matches;
	
	public List<String> getResults(){
		return matches;
	}
	
	public FileFinder(String pattern) {
	    matcher = FileSystems.getDefault()
	            .getPathMatcher("glob:" + pattern);
	    matches = new ArrayList<String>();
	}
	
	// Compares the glob pattern against
	// the file or directory name.
	public void find(Path file) {
	    Path name = file.getFileName();
	    if (name != null && matcher.matches(name)) {
	    	matches.add(file.toString());
	    }
	}
	
	// Invoke the pattern matching
	// method on each file.
	@Override
	public FileVisitResult visitFile(Path file,
	        BasicFileAttributes attrs) {
	    find(file);
	    return CONTINUE;
	}
	
	// Invoke the pattern matching
	// method on each directory.
	@Override
	public FileVisitResult preVisitDirectory(Path dir,
	        BasicFileAttributes attrs) {
	    find(dir);
	    return CONTINUE;
	}
	
	@Override
	public FileVisitResult visitFileFailed(Path file,
	        IOException exc) {
	    return CONTINUE;
	}
}
